package core

import (
	"emerald/log"
	"fmt"
	"github.com/jmoiron/sqlx"
	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"strconv"
)

func CreateCtx(ctx *Ctx) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			cc := &WebContext{Context: c, Ctx: ctx}
			return next(cc)
		}
	}
}

type Ctx struct {
	Db         *sqlx.DB
	AppConfig  *AppConfig
	IrcService IrcService
	settings   *EmeraldSettings
}

func (ctx *Ctx) Copy() *Ctx {
	return &Ctx{Db: ctx.Db, IrcService: ctx.IrcService, settings: ctx.settings}
}

func (ctx *Ctx) Close() {
	if err := ctx.Db.Close(); err != nil {
		logrus.Errorf("error closing database: %v", err)
	}
}

func (ctx *Ctx) Settings() *EmeraldSettings {
	if ctx.settings == nil {
		store := NewStore(ctx)
		es, err := store.Get()
		if err != nil {
			log.Warnf("could not load settings: %v", err)
			es = EmeraldSettings{}
		}
		ctx.settings = &es
	}
	return ctx.settings
}

func (ctx *Ctx) UpdateSettings(settings *EmeraldSettings) error {
	store := NewStore(ctx)
	if err := store.CreateOrUpdate(settings); err != nil {
		return err
	}
	ctx.settings = settings

	return nil
}

type WebContext struct {
	echo.Context
	*Ctx
}

func (ctx *WebContext) ParamAsInt(name string) int {
	p, _ := strconv.Atoi(ctx.Param(name))
	return p
}

func (ctx *WebContext) FormValueAsInt(name string) int {
	p, _ := strconv.Atoi(ctx.FormValue(name))
	return p
}

func (ctx *WebContext) Redirect(code int, name string) error {
	return ctx.Context.Redirect(code, fmt.Sprintf("%s%s", ctx.AppConfig.BasePath, name))
}
