package core

import "time"

type EmeraldSettings struct {
	Id          int    `json:"id"            db:"id"`
	Nick        string `json:"nick"          db:"nick"`
	TempDir     string `json:"tempDir"       db:"temp_dir"`
	DownloadDir string `json:"downloadDir"   db:"download_dir"`
	ShowsDir    string `json:"showsDir"      db:"shows_dir"`
	MoviesDir   string `json:"moviesDir"     db:"movies_dir"`
	StartPort   int    `json:"startPort"     db:"start_port"`
	EndPort     int    `json:"endPort"     db:"end_port"`
	LogFile     string `json:"logFile"       db:"log_file"`
}

type LogEntry struct {
	Id      int       `json:"id" db:"id"`
	Time    time.Time `json:"time" db:"time"`
	Level   string    `json:"level" db:"level"`
	Message string    `json:"message" db:"message"`
}
