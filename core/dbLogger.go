package core

import (
	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
)

// noinspection GoNameStartsWithPackageName
type LogHook struct {
	db *sqlx.DB
}

func (lg *LogHook) Levels() []logrus.Level {
	return logrus.AllLevels
}

func (lg *LogHook) Fire(entry *logrus.Entry) error {
	_, err := lg.db.NamedExec("INSERT INTO logs (time, level, message) VALUES(:time, :level, :message)", &LogEntry{
		Time:    entry.Time,
		Level:   entry.Level.String(),
		Message: entry.Message,
	})

	return err
}

func AddDbLog(ctx *Ctx) {
	logrus.AddHook(&LogHook{db: ctx.Db})
}

func ReadLog(ctx *Ctx) ([]LogEntry, error) {
	return ReadLogUpFrom(ctx, -1)
}

func ReadLogUpFrom(ctx *Ctx, minLogId int) ([]LogEntry, error) {
	var entries []LogEntry
	err := ctx.Db.Select(&entries, "SELECT * FROM logs WHERE id > $1 ORDER BY time DESC LIMIT 200", minLogId)
	return entries, err
}

func CleanUpLog(ctx *Ctx) {
	rs, err := ctx.Db.Exec("DELETE FROM Logs WHERE id < ((SELECT max(id) FROM Logs) - 500);")
	if err != nil {
		logrus.Warnf("error while cleaning log: %v", err)
	} else {
		count, _ := rs.RowsAffected()
		logrus.Debugf("cleaned %d log-entries", count)
	}
}
