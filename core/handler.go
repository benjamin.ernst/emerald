package core

import (
	"github.com/labstack/echo/v4"
	"net/http"
)

func Load(c echo.Context) error {
	ctx := c.(*WebContext).Ctx

	store := NewStore(ctx)
	settings, err := store.Get()
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, settings)
}

func Save(c echo.Context) error {
	ctx := c.(*WebContext).Ctx

	var settings EmeraldSettings
	if err := c.Bind(&settings); err != nil {
		return err
	}
	store := NewStore(ctx)
	if err := store.CreateOrUpdate(&settings); err != nil {
		return err
	}

	//Fixme set download-Limit
	//client := ctx.IrcService.(*irc.Client)
	//client.SetDownloadLimit(settings.MaxDownStream)
	return c.JSON(http.StatusOK, settings)
}
