package core

import "github.com/sirupsen/logrus"

type EventName string

const NewPacket EventName = "NEW_PACKET"
const DownloadComplete EventName = "DOWNLOAD_COMPLETE"

type Event struct {
	Name EventName
	Data interface{}
}

var subscribers = make(map[EventName][]chan Event)

func Publish(name EventName, data interface{}) {
	subscribers, ok := subscribers[name]
	if ok {
		logrus.Debugf("submitting to %d subscribers", len(subscribers))
		for _, sub := range subscribers {
			e := Event{Name: name, Data: data}
			sub <- e
		}
	}
}

func SubscribeEvent(event EventName, bufferSize int) <-chan Event {
	//new subscriber
	subscriber := make(chan Event, bufferSize)

	//add to map
	subscriberList, ok := subscribers[event]
	if !ok {
		subscriberList = make([]chan Event, 0)
	}
	subscriberList = append(subscriberList, subscriber)
	subscribers[event] = subscriberList

	return subscriber
}
