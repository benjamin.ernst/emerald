package core

import (
	"github.com/jmoiron/sqlx"
)

type Store struct {
	db *sqlx.DB
}

func NewStore(c *Ctx) *Store {
	return &Store{db: c.Db}
}

const settingsId = 1

const settingsUpsert = `INSERT INTO settings 
(id, nick, temp_dir, download_dir, shows_dir, movies_dir, log_file, start_port, end_port) 
VALUES (:id, :nick, :temp_dir, :download_dir, :shows_dir, :movies_dir, :log_file, :start_port, :end_port)
ON CONFLICT (id)
DO UPDATE SET nick=:nick, temp_dir=:temp_dir, download_dir=:download_dir, shows_dir=:shows_dir, movies_dir=:movies_dir, log_file=:log_file, start_port=:start_port, end_port=:end_port
WHERE id = :id
`

func (ps *Store) CreateOrUpdate(settings *EmeraldSettings) error {
	settings.Id = settingsId
	_, err := ps.db.NamedExec(settingsUpsert, settings)
	return err
}

func (ps *Store) Get() (EmeraldSettings, error) {
	var settings EmeraldSettings
	if err := ps.db.Get(&settings, "SELECT * FROM settings WHERE id=$1", settingsId); err != nil {
		settings.Id = settingsId
		return settings, nil
	}
	return settings, nil
}
