package files

import (
	"emerald/core"
	"io/ioutil"
	"os"
	"path/filepath"
)

//File represents af File in gotv
type File struct {
	Name   string `json:"name"`
	Folder string `json:"folder"`
	Dir    bool   `json:"dir"`
	Size   int64  `json:"size"`
}

//GetFiles loads all files from the download and temp-dir
func GetFiles(ctx *core.Ctx) []File {
	var files []File
	settings := ctx.Settings()
	//Downloads-Dir
	dir := filepath.FromSlash(settings.DownloadDir)
	currentFiles, _ := ioutil.ReadDir(dir)
	for _, f := range currentFiles {
		file := File{Name: f.Name(), Size: f.Size(), Folder: "Downloads", Dir: f.IsDir()}
		files = append(files, file)
	}

	//Temp-Dir
	dir = filepath.FromSlash(settings.TempDir)
	currentFiles, _ = ioutil.ReadDir(dir)
	for _, f := range currentFiles {
		file := File{Name: f.Name(), Size: f.Size(), Folder: "Temp", Dir: f.IsDir()}
		files = append(files, file)
	}

	return files
}

//DeleteFiles deletes the given files
func DeleteFiles(ctx *core.Ctx, files []File) error {
	settings := ctx.Settings()
	for _, f := range files {
		var baseDir string
		if f.Folder == "Downloads" {
			baseDir = settings.DownloadDir
		} else {
			baseDir = settings.TempDir
		}

		absoluteFile := filepath.FromSlash(baseDir + "/" + f.Name)

		if f.Dir {
			err := os.RemoveAll(absoluteFile)
			if err != nil {
				return err
			}
		} else {
			err := os.Remove(absoluteFile)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

//MoveFilesToMovies moves the given files to the MoviesFolder
func MoveFilesToMovies(ctx *core.Ctx, files []File) error {
	settings := ctx.Settings()
	for _, f := range files {
		var baseDir string
		if f.Folder == "Downloads" {
			baseDir = settings.DownloadDir
		} else {
			baseDir = settings.TempDir
		}

		absoluteFile := filepath.FromSlash(baseDir + "/" + f.Name)
		destination := filepath.FromSlash(settings.MoviesDir + "/" + f.Name)

		err := os.Rename(absoluteFile, destination)
		if err != nil {
			return err
		}
	}

	return nil
}
