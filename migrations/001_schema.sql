-- +goose Up
CREATE TABLE IF NOT EXISTS settings (
    id           INTEGER PRIMARY KEY,
    nick         VARCHAR(250) DEFAULT '',
    temp_dir     VARCHAR(250) DEFAULT '',
    download_dir VARCHAR(250) DEFAULT '',
    shows_dir    VARCHAR(250) DEFAULT '',
    movies_dir   VARCHAR(250) DEFAULT '',
    log_file     VARCHAR(250) DEFAULT ''
);

CREATE TABLE IF NOT EXISTS logs (
    id           INTEGER PRIMARY KEY AUTOINCREMENT,
    time         TIMESTAMP NOT NULL,
    level        VARCHAR(50) NOT NULL,
    message      VARCHAR(500) NOT NULL
);

CREATE TABLE IF NOT EXISTS packets (
    id         VARCHAR(250) PRIMARY KEY,
    packet_id  VARCHAR(250) DEFAULT '',
    size       VARCHAR(100) DEFAULT '',
    name       VARCHAR(250) DEFAULT '',
    bot        VARCHAR(250) DEFAULT '',
    channel    VARCHAR(250) DEFAULT '',
    server     VARCHAR(250) DEFAULT '',
    date       TIMESTAMP
);

CREATE TABLE IF NOT EXISTS servers (
    id         INTEGER PRIMARY KEY AUTOINCREMENT,
    name       VARCHAR(250) DEFAULT '',
    port       VARCHAR(10)  DEFAULT '',
    ssl        BOOLEAN DEFAULT 0
);

CREATE TABLE IF NOT EXISTS channels (
    server_id  INTEGER NOT NULL,
    name       VARCHAR(250) DEFAULT '',
    PRIMARY KEY (server_id, name),
    FOREIGN KEY(server_id) REFERENCES servers(id)
);

DROP TABLE IF EXISTS shows;
CREATE TABLE IF NOT EXISTS shows (
    id          INTEGER PRIMARY KEY,
    name        VARCHAR(100)  DEFAULT '',
    banner      VARCHAR(500)  DEFAULT '',
    poster      VARCHAR(500)  DEFAULT '',
    first_aired TIMESTAMP NOT NULL,
    overview    VARCHAR(5000) DEFAULT '',
    search_name VARCHAR(100)  DEFAULT '',
    folder      VARCHAR(250)  DEFAULT ''
);

DROP TABLE IF EXISTS episodes;
CREATE TABLE IF NOT EXISTS episodes (
    id              INTEGER PRIMARY KEY,
    show_id         INTEGER NOT NULL,
    name            VARCHAR(250)  DEFAULT '',
    first_aired     TIMESTAMP NOT NULL,
    overview        VARCHAR(5000) DEFAULT '',
    file_name       VARCHAR(250)  DEFAULT '',
    episode_number  INTEGER NOT NULL,
    season_number   INTEGER NOT NULL,
    still           VARCHAR(500)  DEFAULT '',
    FOREIGN KEY(show_id) REFERENCES shows(id)
);

-- +goose Down
DROP TABLE settings;
DROP TABLE logs;
DROP TABLE packets;
DROP TABLE servers;
DROP TABLE channels;
DROP TABLE shows;
DROP TABLE episodes;