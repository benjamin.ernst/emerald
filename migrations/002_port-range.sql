-- +goose Up
ALTER TABLE settings ADD COLUMN start_port INTEGER default 0;
ALTER TABLE settings ADD COLUMN end_port INTEGER default 0;

-- +goose Down
ALTER TABLE settings DROP COLUMN end_port;
ALTER TABLE settings DROP COLUMN start_port;