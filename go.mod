module emerald

go 1.20

require (
	github.com/cyruzin/golang-tmdb v1.5.0
	github.com/dustin/go-humanize v1.0.1
	github.com/fluffle/goirc v1.3.0
	github.com/glendc/go-external-ip v0.1.0
	github.com/google/uuid v1.3.1
	github.com/gorilla/websocket v1.5.0
	github.com/jmoiron/sqlx v1.3.5
	github.com/labstack/echo/v4 v4.10.2
	github.com/mattn/go-sqlite3 v1.14.16
	github.com/pressly/goose/v3 v3.15.1
	github.com/robfig/cron v1.2.0
	github.com/sirupsen/logrus v1.9.3
)

require (
	github.com/emersion/go-sasl v0.0.0-20220912192320-0145f2c60ead // indirect
	github.com/golang-jwt/jwt v3.2.2+incompatible // indirect
	github.com/golang/mock v1.5.0 // indirect
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/labstack/gommon v0.4.0 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	github.com/modern-go/concurrent v0.0.0-20180228061459-e0a39a4cb421 // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasttemplate v1.2.2 // indirect
	golang.org/x/crypto v0.13.0 // indirect
	golang.org/x/net v0.10.0 // indirect
	golang.org/x/sys v0.13.0 // indirect
	golang.org/x/text v0.13.0 // indirect
	golang.org/x/time v0.3.0 // indirect
)
