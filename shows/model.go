package shows

import (
	"time"
)

type ImageType string

const Banner ImageType = "banner"
const Poster ImageType = "poster"

// Show
type Show struct {
	Id         int64     `json:"id" db:"id"`
	Name       string    `json:"name" db:"name"`
	Banner     string    `json:"banner" db:"banner"`
	Poster     string    `json:"poster" db:"poster"`
	FirstAired time.Time `json:"firstAired" db:"first_aired"`
	Overview   string    `json:"overview" db:"overview"`
	SearchName string    `json:"searchName" db:"search_name"`
	Folder     string    `json:"folder" db:"folder"`
}

// Episode
type Episode struct {
	Id            int64     `json:"id" db:"id"`
	ShowId        int64     `json:"showId" db:"show_id"`
	Name          string    `json:"name" db:"name"`
	FirstAired    time.Time `json:"firstAired" db:"first_aired"`
	Overview      string    `json:"overview" db:"overview"`
	Filename      string    `json:"filename" db:"file_name"`
	EpisodeNumber int       `json:"episodeNumber" db:"episode_number"`
	SeasonNumber  int       `json:"seasonNumber" db:"season_number"`
	Still         string    `json:"still" db:"still"`
}

// RecentEpisode
type RecentEpisode struct {
	Episode Episode `json:"episode"`
	Show    Show    `json:"show"`
}
