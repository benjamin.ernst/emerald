package shows

import (
	"reflect"
	"testing"
)

func Test_parseShow(t *testing.T) {
	type args struct {
		absoluteFile string
	}
	tests := []struct {
		name string
		args args
		want *ShowInfo
	}{
		{
			name: "01",
			args: args{absoluteFile: "Modern Family - 11x07 - The Last Thanksgiving.mkv"},
			want: &ShowInfo{
				Name:    "Modern Family",
				Season:  11,
				Episode: 7,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := parseShow(tt.args.absoluteFile); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parseShow() = %v, want %v", got, tt.want)
			}
		})
	}
}
