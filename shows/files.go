package shows

import (
	"emerald/core"
	"emerald/log"
	"fmt"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
)

var (
	seRegex, _ = regexp.Compile(`(.*?)[.\s][sS](\d{2})[eE](\d{2}).*`)
	xRegex, _  = regexp.Compile(`(.*?)[.\s](\d{1,2})[xX](\d{2}).*`)
)

// ShowInfo Info about a show
type ShowInfo struct {
	Name    string
	Season  int
	Episode int
}

func SubscribeDownloadCompleted(c *core.Ctx) {
	incoming := core.SubscribeEvent(core.DownloadComplete, 5)

	go func() {
		for event := range incoming {
			fileName, ok := event.Data.(string)
			if !ok {
				log.Errorf("[shows] could not cast event-data %v as string", event.Data)
			}
			log.Infof("[shows] new download completed: %s", fileName)

			ScanDownloadDir(c)
		}
	}()
	log.Infof("[shows] listening to '%s' events...", core.DownloadComplete)
}

// ScanDownloadDir tries to move episodes from the download-folder
func ScanDownloadDir(c *core.Ctx) {
	// iterate over files in download-Dir
	err := filepath.Walk(c.Settings().DownloadDir, func(file string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if !info.IsDir() {
			MoveEpisode(c, file)
		}
		return nil
	})
	if err != nil {
		log.Errorf("Error while updating episodes: %v", err)
	}

}

// MoveEpisode moves the epsiode to its season folder
func MoveEpisode(c *core.Ctx, file string) {
	info := parseShow(file)

	if info != nil {
		show, episode := getShowData(c, info)

		if show == nil || episode == nil {
			//error
			return
		}

		// create output file
		fileEnding := file[strings.LastIndex(file, "."):]
		destinationFolder := c.Settings().ShowsDir + "/" + show.Folder + "/Season " + strconv.Itoa(int(episode.SeasonNumber)) + "/"
		fileName := sanitizeFilename(fmt.Sprintf("%s.S%02dE%02d.%s", show.Name, episode.SeasonNumber, episode.EpisodeNumber, episode.Name))

		//create all directories
		err := os.MkdirAll(destinationFolder, 0755)
		if err != nil {
			log.Errorf("Error making dir: %s", err)
		}

		//move episode to destination
		srcFile := filepath.FromSlash(file)
		destFile := filepath.FromSlash(destinationFolder + fileName + fileEnding)
		err = os.Rename(srcFile, destFile)
		if err != nil {
			log.Errorf("Error while moving epsiode to destination: %s", err)
			return
		}

		log.Infof("Moved Episode '%s' to folder '%s'", fileName, destinationFolder)
	}
}

func getShowData(c *core.Ctx, info *ShowInfo) (*Show, *Episode) {
	store := NewStore(c)
	show, err := store.GetShowBySearchName(info.Name)
	if err != nil {
		log.Errorf("could not find show: %v", info.Name)
		return nil, nil
	}

	// find Episode
	var episode *Episode
	seasons, err := LoadShowEpisodes(c, show.Id)
	episodes := seasons[strconv.Itoa(int(info.Season))]
	for i := range episodes {
		if int(episodes[i].EpisodeNumber) == info.Episode {
			episode = &episodes[i]
		}
	}

	if episode == nil {
		log.Errorf("could not find episode: %v for show %v", info.Episode, info.Name)
		return nil, nil
	}

	return &show, episode
}

func sanitizeFilename(filename string) string {
	// Remove all strange characters
	seps, err := regexp.Compile(`[&_=+:/]`)
	if err == nil {
		filename = seps.ReplaceAllString(filename, "")
	}
	filename = strings.ReplaceAll(filename, " ", ".")

	return filename
}

func parseShow(absoluteFile string) *ShowInfo {
	info := new(ShowInfo)

	// Replace all \ with /
	absoluteFile = strings.Replace(absoluteFile, "\\", "/", -1)

	// cut off the path
	file := absoluteFile[strings.LastIndex(absoluteFile, "/")+1:]

	// Replace all _ with dots
	file = strings.Replace(file, " - ", ".", -1)
	file = strings.Replace(file, "_", ".", -1)
	file = strings.Replace(file, " ", ".", -1)

	result := seRegex.FindStringSubmatch(file)
	if result != nil {
		info.Name = strings.Replace(result[1], ".", " ", -1)
		info.Season, _ = strconv.Atoi(result[2])
		info.Episode, _ = strconv.Atoi(result[3])
	} else {
		// try other pattern
		result = xRegex.FindStringSubmatch(file)
		if result != nil {
			info.Name = strings.Replace(result[1], ".", " ", -1)
			info.Season, _ = strconv.Atoi(result[2])
			info.Episode, _ = strconv.Atoi(result[3])
		} else {
			return nil
		}
	}

	return info
}
