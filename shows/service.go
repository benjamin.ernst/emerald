package shows

import (
	"emerald/core"
	tmdb "github.com/cyruzin/golang-tmdb"
	"strconv"
	"time"
)

const dateFormat = "2006-01-02"

type ShowService struct {
	client *tmdb.Client
}

func NewShowService(ctx *core.Ctx) (*ShowService, error) {
	tmdbClient, err := tmdb.Init(ctx.AppConfig.TmdbApiKey)
	if err != nil {
		return nil, err
	}
	tmdbClient.SetClientAutoRetry()
	return &ShowService{client: tmdbClient}, nil
}

func (s *ShowService) SearchShows(query string) ([]Show, error) {
	search, err := s.client.GetSearchTVShow(query, nil)
	if err != nil {
		return nil, err
	}

	shows := make([]Show, search.TotalResults)
	for i, searchResult := range search.Results {
		show := Show{}
		show.Id = searchResult.ID
		show.Name = searchResult.Name
		show.SearchName = searchResult.Name
		show.Folder = searchResult.Name
		show.FirstAired = parseDate(searchResult.FirstAirDate)
		show.Overview = searchResult.Overview

		show.Poster = tmdb.GetImageURL(searchResult.PosterPath, tmdb.W780)
		show.Banner = tmdb.GetImageURL(searchResult.BackdropPath, tmdb.W780)

		shows[i] = show
	}
	return shows, nil
}

func (s *ShowService) LoadShow(id int) (Show, error) {
	show := Show{}

	tvShow, err := s.client.GetTVDetails(id, nil)
	if err != nil {
		return show, err
	}

	show.Id = tvShow.ID
	show.Name = tvShow.Name
	show.SearchName = tvShow.Name
	show.Folder = tvShow.Name
	show.FirstAired = parseDate(tvShow.FirstAirDate)
	show.Overview = tvShow.Overview
	show.Poster = tmdb.GetImageURL(tvShow.PosterPath, tmdb.W780)
	show.Banner = tmdb.GetImageURL(tvShow.BackdropPath, tmdb.W780)

	return show, nil
}

func parseDate(date string) time.Time {
	t, _ := time.Parse(dateFormat, date)
	return t
}

func (s *ShowService) UpdateShow(ctx *core.Ctx, show *Show) error {
	store := NewStore(ctx)

	//update episodes
	tvShow, err := s.client.GetTVDetails(int(show.Id), nil)
	if err != nil {
		return err
	}

	episodes := make([]*Episode, 0)
	for _, season := range tvShow.Seasons {
		if season.SeasonNumber <= 0 {
			continue
		}

		details, err := s.client.GetTVSeasonDetails(int(show.Id), season.SeasonNumber, nil)
		if err != nil {
			return err
		}

		for _, eps := range details.Episodes {
			episode := &Episode{}
			episode.Id = eps.ID
			episode.ShowId = eps.ShowID
			episode.Name = eps.Name
			episode.FirstAired = parseDate(eps.AirDate)
			episode.Overview = eps.Overview
			episode.Filename = eps.Name
			episode.EpisodeNumber = eps.EpisodeNumber
			episode.SeasonNumber = eps.SeasonNumber
			episode.Still = tmdb.GetImageURL(eps.StillPath, tmdb.W780)
			episodes = append(episodes, episode)
		}
	}

	err = store.SaveEpisodes(episodes)
	return err
}

func LoadShowEpisodes(ctx *core.Ctx, showId int64) (map[string][]Episode, error) {
	store := NewStore(ctx)
	episodes, err := store.LoadEpisodes(showId)
	if err != nil {
		return nil, err
	}

	seasons := make(map[string][]Episode)
	for _, episode := range episodes {
		seasonEps := seasons[strconv.Itoa(int(episode.SeasonNumber))]
		if seasonEps == nil {
			seasonEps = make([]Episode, 0)
		}

		seasonEps = append(seasonEps, episode)

		seasons[strconv.Itoa(int(episode.SeasonNumber))] = seasonEps
	}

	return seasons, nil
}
