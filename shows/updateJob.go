package shows

import (
	"emerald/core"
	"github.com/sirupsen/logrus"
	"log"

	"github.com/robfig/cron"
)

// UpdateJob updates all episodes in the night
type UpdateJob struct {
	ctx *core.Ctx
}

// StartJob starts the UpdateJob
func StartJob(ctx *core.Ctx) error {
	uj := &UpdateJob{ctx: ctx}

	c := cron.New()
	if err := c.AddFunc("@daily", uj.updateShows); err != nil {
		return err
	}
	c.Start()
	return nil
}

func (uj *UpdateJob) updateShows() {
	store := NewStore(uj.ctx)
	showService, err := NewShowService(uj.ctx)
	if err != nil {
		logrus.Errorf("[UpdateJob] error while creating showService: %v", err)
	}

	shows, err := store.GetShows()
	if err != nil {
		log.Printf("Error while updating shows: %v", err)
	}
	for _, show := range shows {
		if err := showService.UpdateShow(uj.ctx, &show); err != nil {
			logrus.Warnf("[UpdateJob] error while updating show: %v", err)
		}
	}
	log.Printf("Updated %d shows", len(shows))
}
