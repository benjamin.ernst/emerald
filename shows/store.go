package shows

import (
	"emerald/core"
	"github.com/jmoiron/sqlx"
	"strconv"
	"time"
)

type Store struct {
	db *sqlx.DB
}

func NewStore(c *core.Ctx) *Store {
	return &Store{db: c.Db}
}

const showsUpsert = `INSERT INTO shows 
(id, name, banner, poster, first_aired, overview, search_name, folder) 
VALUES (:id, :name, :banner, :poster, :first_aired, :overview, :search_name, :folder)
ON CONFLICT (id)
DO UPDATE SET name=:name, banner=:banner, poster=:poster, first_aired=:first_aired, overview=:overview, search_name=:search_name, folder=:folder
WHERE id = :id
`

func (ps *Store) SaveShow(show *Show) error {
	_, err := ps.db.NamedExec(showsUpsert, show)
	return err
}

func (ps *Store) GetShow(id int64) (Show, error) {
	var show Show
	if err := ps.db.Get(&show, "SELECT * FROM shows WHERE id=$1", id); err != nil {
		return show, err
	}
	return show, nil
}

func (ps *Store) GetShowBySearchName(name string) (Show, error) {
	var show Show
	if err := ps.db.Get(&show, "SELECT * FROM shows WHERE search_name like $1", name); err != nil {
		return show, err
	}
	return show, nil
}

func (ps *Store) GetShows() ([]Show, error) {
	var shows []Show
	err := ps.db.Select(&shows, "SELECT * FROM shows")
	return shows, err
}

func (ps *Store) DeleteShow(show *Show) error {
	tx, err := ps.db.Beginx()
	if err != nil {
		return err
	}

	_, err = tx.Exec("DELETE FROM shows where id = $1", show.Id)
	if err != nil {
		return err
	}

	_, err = tx.Exec("DELETE FROM episodes where show_id = $1", show.Id)
	if err != nil {
		return err
	}
	return tx.Commit()
}

// EPISODES
func (ps *Store) LoadEpisodes(showId int64) ([]Episode, error) {
	var episodes []Episode
	err := ps.db.Select(&episodes, "SELECT * FROM episodes WHERE show_id=$1 ORDER BY first_aired DESC", showId)
	return episodes, err
}

func (ps *Store) LoadRecentEpisodes(duration string) ([]RecentEpisode, error) {
	days, _ := strconv.Atoi(duration)
	delay, _ := time.ParseDuration("-" + strconv.Itoa(days*24) + "h")
	pastDate := time.Now().Add(delay)
	yesterday := time.Now().AddDate(0, 0, -1)

	var episodes []Episode
	err := ps.db.Select(&episodes, "SELECT * FROM episodes WHERE first_aired > $1 AND first_aired < $2  ORDER BY first_aired DESC", pastDate, yesterday)
	if err != nil {
		return nil, err
	}

	recentEpisodes := make([]RecentEpisode, len(episodes))
	for i, episode := range episodes {
		show, err := ps.GetShow(episode.ShowId)
		if err != nil {
			return recentEpisodes, err
		}

		recentEpisode := RecentEpisode{}
		recentEpisode.Episode = episode
		recentEpisode.Show = show
		recentEpisodes[i] = recentEpisode
	}

	return recentEpisodes, nil
}

const episodesUpsert = `INSERT INTO episodes 
(id, name, show_id, first_aired, overview, file_name, episode_number, season_number, still) 
VALUES (:id, :name, :show_id, :first_aired, :overview, :file_name, :episode_number, :season_number, :still)
ON CONFLICT (id)
DO UPDATE SET name=:name, show_id=:show_id, first_aired=:first_aired, overview=:overview, file_name=:file_name, episode_number=:episode_number, season_number=:season_number, still=:still
WHERE id = :id
`

func (ps *Store) SaveEpisodes(episodes []*Episode) error {
	tx, err := ps.db.Beginx()
	if err != nil {
		return err
	}

	for _, episode := range episodes {
		_, err := tx.NamedExec(episodesUpsert, episode)
		if err != nil {
			return err
		}
	}

	return tx.Commit()
}
