package main

import (
	"embed"
	"emerald/core"
	"emerald/irc"
	"emerald/shows"
	"emerald/web"
	"emerald/web/views"
	"fmt"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/sirupsen/logrus"
	"strings"
	"time"
)

//go:embed migrations/*.sql
var embedMigrations embed.FS

var buildVersion string

func main() {
	ctx := core.InitApp(buildVersion, embedMigrations)
	defer ctx.Close()

	ctx.IrcService = irc.NewClient(ctx)

	//subscriptions
	irc.SubscribeNewPacket(ctx)
	shows.SubscribeDownloadCompleted(ctx)

	startCleanUp(ctx)

	e := echo.New()
	e.Renderer = web.NewTemplate(ctx)
	e.Debug = true
	//e.Logger.SetLevel(log.DEBUG)
	//e.Use(middleware.Logger())
	e.Use(middleware.CORS())
	e.Use(core.CreateCtx(ctx))

	root := e.Group(ctx.AppConfig.BasePath)

	root.Static("/assets", "./web/assets/build")

	root.GET("/", views.Index)
	root.GET("/servers", views.ServerEdit)
	root.GET("/servers/:id", views.ServerEdit)
	root.POST("/servers", views.ServerCreate)
	root.POST("/servers/:id", views.ServerUpdate)
	root.POST("/servers/:id/delete", views.ServerDelete)
	root.POST("/servers/:id/connect", views.ServerConnect)
	root.POST("/servers/:id/disconnect", views.ServerDisconnect)

	root.GET("/search", views.Search)

	root.GET("/files", views.Files)
	root.POST("/files/update", views.Update)
	root.POST("/files/:dir/:file/move", views.Move)
	root.POST("/files/:dir/:file/delete", views.Delete)

	root.POST("/downloads/:id/start", views.StartDownload)
	root.POST("/downloads/:id/resume", views.ResumeDownload)
	root.POST("/downloads/:id/stop", views.StopDownload)
	root.POST("/downloads/:id/cancel", views.CancelDownload)
	root.POST("/downloads/clear", views.ClearCompleted)
	root.GET("/downloads", views.Downloads)
	root.GET("/downloads/status", views.DownloadStatus)

	root.GET("/shows/recent", views.ShowsRecent)
	root.GET("/shows/list", views.ShowsList)
	root.GET("/shows/search", views.ShowsSearch)
	root.GET("/shows/:id", views.ShowsEdit)
	root.POST("/shows/:id", views.ShowSave)
	root.POST("/shows/:id/delete", views.ShowDelete)

	root.GET("/settings", views.Settings)
	root.POST("/settings", views.SettingsUpdate)

	root.GET("/log", views.Log)
	root.GET("/log/update/:maxLogId", views.UpdateLog)

	root.GET("/metrics", views.Metrics)

	//Start all jobs
	if err := shows.StartJob(ctx); err != nil {
		logrus.Warnf("error starting jobs: %v", err)
	}

	// Listen and server on 0.0.0.0:8080
	e.Logger.Fatal(e.Start(fmt.Sprintf(":%s", ctx.AppConfig.Port)))
}

func startCleanUp(ctx *core.Ctx) {
	ticker := time.NewTicker(15 * time.Minute)
	store := irc.NewStore(ctx)
	go func() {
		for {
			select {
			case _ = <-ticker.C:
				core.CleanUpLog(ctx)
				if err := store.DeleteOldPackets(); err != nil && !strings.Contains(err.Error(), "not found") {
					logrus.Warnf("error while deleting old packets: %v", err)
				}
			}
		}
	}()
}
