package util

import (
	"encoding/binary"
	"net"
	"strconv"
	"strings"

	externalip "github.com/glendc/go-external-ip"
)

func GetExternalIP() (net.IP, error) {
	// Create the default consensus,
	// using the default configuration and no logger.
	consensus := externalip.DefaultConsensus(nil, nil)

	// By default Ipv4 or Ipv6 is returned,
	// use the function below to limit yourself to IPv4,
	// or pass in `6` instead to limit yourself to IPv6.
	if err := consensus.UseIPProtocol(4); err != nil {
		return nil, err
	}

	// Get your IP,
	// which is never <nil> when err is <nil>.
	return consensus.ExternalIP()
}

// Convert uint to net.IP
func Int2IP(ip string) net.IP {
	if strings.Contains(ip, ":") {
		return net.ParseIP(ip)
	}

	ipnr, _ := strconv.ParseInt(ip, 0, 64)
	var bytes [4]byte
	bytes[0] = byte(ipnr & 0xFF)
	bytes[1] = byte((ipnr >> 8) & 0xFF)
	bytes[2] = byte((ipnr >> 16) & 0xFF)
	bytes[3] = byte((ipnr >> 24) & 0xFF)

	return net.IPv4(bytes[3], bytes[2], bytes[1], bytes[0])
}

func IP2Int(ip net.IP) uint32 {
	if len(ip) == 16 {
		return binary.BigEndian.Uint32(ip[12:16])
	}
	return binary.BigEndian.Uint32(ip)
}
