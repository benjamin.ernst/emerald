package views

import (
	"emerald/core"
	"emerald/irc"
	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"net/http"
)

type serverWithStatus struct {
	*irc.Server
	Connected bool
}

func ServerList(c echo.Context) error {
	ctx := c.(*core.WebContext).Ctx

	store := irc.NewStore(ctx)
	servers, err := store.GetServers()
	if err != nil {
		return err
	}

	client := ctx.IrcService.(*irc.Client)
	serversWithStatus := make([]serverWithStatus, len(servers))
	for i, server := range servers {
		connected := client.IsServerConnected(server)
		serversWithStatus[i] = serverWithStatus{Server: server, Connected: connected}
	}

	data := struct {
		Servers []serverWithStatus
	}{Servers: serversWithStatus}

	return c.Render(http.StatusOK, "index.html", &data)
}

func ServerEdit(c echo.Context) error {
	ctx := c.(*core.WebContext)

	server := &irc.Server{}
	id := c.Param("id")

	if id != "" {
		store := irc.NewStore(ctx.Ctx)
		var err error
		server, err = store.GetServer(ctx.ParamAsInt("id"))
		if err != nil {
			return err
		}
	}

	return c.Render(http.StatusOK, "server-edit.html", server)
}

func ServerCreate(c echo.Context) error {
	ctx := c.(*core.WebContext)
	store := irc.NewStore(ctx.Ctx)

	server := irc.Server{}
	server.Name = c.FormValue("name")
	server.Port = c.FormValue("port")
	server.Ssl = c.FormValue("ssl") == "on"

	params, err := c.FormParams()
	if err != nil {
		return err
	}
	channels := params["channel"]
	server.Channels = make([]irc.Channel, len(channels))
	for i, channel := range channels {
		server.Channels[i] = irc.Channel{Name: channel}
	}

	if err := store.CreateServer(&server); err != nil {
		logrus.Errorf("error creating server: %v", err)
		return err
	}

	return ctx.Redirect(http.StatusFound, "/")
}

func ServerUpdate(c echo.Context) error {
	ctx := c.(*core.WebContext)
	store := irc.NewStore(ctx.Ctx)

	id := ctx.ParamAsInt("id")
	server, err := store.GetServer(id)
	if err != nil {
		return err
	}
	server.Name = c.FormValue("name")
	server.Port = c.FormValue("port")
	server.Ssl = c.FormValue("ssl") == "on"

	params, err := c.FormParams()
	if err != nil {
		return err
	}
	channels := params["channel"]
	server.Channels = make([]irc.Channel, len(channels))
	for i, channel := range channels {
		server.Channels[i] = irc.Channel{Name: channel}
	}

	if err := store.UpdateServer(server); err != nil {
		logrus.Errorf("error updating server: %v", err)
		return err
	}

	return ctx.Redirect(http.StatusFound, "/")
}

func ServerDelete(c echo.Context) error {
	ctx := c.(*core.WebContext)
	store := irc.NewStore(ctx.Ctx)

	id := ctx.ParamAsInt("id")
	server, err := store.GetServer(id)
	if err != nil {
		return err
	}

	if err := store.DeleteServer(server); err != nil {
		logrus.Errorf("error deleting server: %v", err)
		return err
	}

	return ctx.Redirect(http.StatusFound, "/")
}

func ServerConnect(c echo.Context) error {
	ctx := c.(*core.WebContext)
	store := irc.NewStore(ctx.Ctx)

	id := ctx.ParamAsInt("id")
	server, err := store.GetServer(id)
	if err != nil {
		return err
	}

	client := ctx.IrcService.(*irc.Client)
	client.ConnectServer(ctx.Ctx, server)

	return ctx.Redirect(http.StatusFound, "/")
}

func ServerDisconnect(c echo.Context) error {
	ctx := c.(*core.WebContext)
	store := irc.NewStore(ctx.Ctx)

	id := ctx.ParamAsInt("id")
	server, err := store.GetServer(id)
	if err != nil {
		return err
	}

	client := ctx.IrcService.(*irc.Client)
	client.DisConnectServer(ctx.Ctx, server)

	return ctx.Redirect(http.StatusFound, "/")
}
