package views

import (
	"emerald/core"
	"github.com/gorilla/websocket"
	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"net/http"
	"time"
)

func Log(c echo.Context) error {
	ctx := c.(*core.WebContext).Ctx
	logEntries, err := core.ReadLog(ctx)
	if err != nil {
		return err
	}

	maxLogId := 0
	if len(logEntries) > 0 {
		maxLogId = logEntries[0].Id
	}

	return c.Render(http.StatusOK, "log.html", map[string]interface{}{
		"logs":     logEntries,
		"maxLogId": maxLogId,
	})
}

func UpdateLog(c echo.Context) error {
	ws, err := upgrader.Upgrade(c.Response(), c.Request(), nil)
	if err != nil {
		return err
	}

	ctx := c.(*core.WebContext)
	maxLogId := ctx.ParamAsInt("maxLogId")

	go updateLogs(ws, ctx.Ctx, maxLogId)
	return nil
}

func updateLogs(ws *websocket.Conn, ctx *core.Ctx, maxLogId int) {
	ticker := time.NewTicker(1 * time.Second)
	defer func() {
		ticker.Stop()
		err := ws.Close()
		if err != nil {
			logrus.Warnf("error closing websocket: %v", err)
		}
	}()
	for {
		select {
		case <-ticker.C:
			logEntries, err := core.ReadLogUpFrom(ctx, maxLogId)
			if err != nil {
				logrus.Errorf("error reading logs: %v", err)
				return
			}

			if len(logEntries) > 0 {
				maxLogId = logEntries[0].Id
			}

			if err := ws.WriteJSON(logEntries); err != nil {
				logrus.Debugf("error writing logs to web-socket: %v", err)
				return
			}
		}
	}
}
