package views

import (
	"emerald/core"
	"emerald/irc"
	"github.com/gorilla/websocket"
	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"net/http"
	"time"
)

func Downloads(c echo.Context) error {
	ctx := c.(*core.WebContext).Ctx
	client := ctx.IrcService.(*irc.Client)

	downloads := getDownloads(client)

	return c.Render(http.StatusOK, "downloads.html", downloads)
}

var (
	upgrader = websocket.Upgrader{}
)

func DownloadStatus(c echo.Context) error {
	ws, err := upgrader.Upgrade(c.Response(), c.Request(), nil)
	if err != nil {
		return err
	}

	ctx := c.(*core.WebContext).Ctx
	client := ctx.IrcService.(*irc.Client)
	go updateStatus(ws, client)
	return nil
}

func ResumeDownload(c echo.Context) error {
	ctx := c.(*core.WebContext)
	client := ctx.IrcService.(*irc.Client)
	download, err := client.GetDownload(c.Param("id"))
	if err != nil {
		return err
	}
	client.ResumeDownload(download)

	return ctx.Redirect(http.StatusFound, "/downloads")
}

func StopDownload(c echo.Context) error {
	ctx := c.(*core.WebContext)
	client := ctx.IrcService.(*irc.Client)
	download, err := client.GetDownload(c.Param("id"))
	if err != nil {
		return err
	}
	client.StopDownload(download)

	return ctx.Redirect(http.StatusFound, "/downloads")
}

func CancelDownload(c echo.Context) error {
	ctx := c.(*core.WebContext)
	client := ctx.IrcService.(*irc.Client)
	download, err := client.GetDownload(c.Param("id"))
	if err != nil {
		return err
	}
	client.CancelDownload(download.ID)

	return ctx.Redirect(http.StatusFound, "/downloads")
}

func ClearCompleted(c echo.Context) error {
	ctx := c.(*core.WebContext)
	client := ctx.IrcService.(*irc.Client)
	for _, download := range client.ListDownloads() {
		if download.Status == "COMPLETE" {
			client.CancelDownload(download.ID)
		}
	}

	return ctx.Redirect(http.StatusFound, "/downloads")
}

func updateStatus(ws *websocket.Conn, client *irc.Client) {
	ticker := time.NewTicker(1 * time.Second)
	defer func() {
		ticker.Stop()
		err := ws.Close()
		if err != nil {
			logrus.Warnf("error closing websocket: %v", err)
		}
	}()
	for {
		select {
		case <-ticker.C:
			downloads := getDownloads(client)
			err := ws.WriteJSON(downloads)
			if err != nil {
				logrus.Debugf("error writing web-socket message: %v", err)
				return
			}
		}
	}
}

func getDownloads(client *irc.Client) []*irc.Download {
	downloads := client.ListDownloads()

	//test
	//downloads = append(downloads, createExampleDownload("123"))
	//
	//downloads = append(downloads, createExampleDownload("124"))
	//
	//downloads = append(downloads, createExampleDownload("125"))
	//
	//downloads = append(downloads, createExampleDownload("126"))

	for _, download := range downloads {
		download.FillDisplayData()
	}

	return downloads
}
