package views

import (
	"emerald/core"
	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"net/http"
)

func Settings(c echo.Context) error {
	ctx := c.(*core.WebContext).Ctx
	return c.Render(http.StatusOK, "settings.html", ctx.Settings())
}

func SettingsUpdate(c echo.Context) error {
	ctx := c.(*core.WebContext)
	settings := ctx.Settings()

	settings.Nick = c.FormValue("nick")
	settings.TempDir = c.FormValue("tempDir")
	settings.DownloadDir = c.FormValue("downloadDir")
	settings.ShowsDir = c.FormValue("showsDir")
	settings.MoviesDir = c.FormValue("moviesDir")
	settings.StartPort = ctx.FormValueAsInt("startPort")
	settings.EndPort = ctx.FormValueAsInt("endPort")

	if err := ctx.UpdateSettings(settings); err != nil {
		logrus.Errorf("error updating settings: %v", err)
		return err
	}

	return ctx.Redirect(http.StatusFound, "/")
}
