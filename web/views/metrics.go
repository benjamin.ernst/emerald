package views

import (
	"emerald/core"
	"emerald/irc"
	"emerald/log"
	"github.com/labstack/echo/v4"
	"net/http"
	"time"
)

type metrics struct {
	Count int
}

func Metrics(c echo.Context) error {
	ctx := c.(*core.WebContext).Ctx
	store := irc.NewStore(ctx)

	start := time.Now()
	count, err := store.CountPackets()
	if err != nil {
		log.Warnf("error counting packets: %v", err)
	}
	log.Infof("count took: %s", time.Since(start))

	m := metrics{Count: count}

	return c.Render(http.StatusOK, "metrics.html", m)
}
