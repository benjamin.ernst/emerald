package views

import (
	"emerald/core"
	"emerald/log"
	"emerald/shows"
	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"net/http"
)

func ShowsRecent(c echo.Context) error {
	ctx := c.(*core.WebContext)
	store := shows.NewStore(ctx.Ctx)

	episodes, err := store.LoadRecentEpisodes("7")
	if err != nil {
		log.Warnf("error loading recent episodes: %v", err)
	}

	return c.Render(http.StatusOK, "shows-recent.html", episodes)
}

func ShowsList(c echo.Context) error {
	ctx := c.(*core.WebContext)
	store := shows.NewStore(ctx.Ctx)

	showResults, err := store.GetShows()
	if err != nil {
		return err
	}

	return c.Render(http.StatusOK, "shows-list.html", showResults)
}

type showSearchResult struct {
	Query string
	Shows []shows.Show
}

func ShowsSearch(c echo.Context) error {
	ctx := c.(*core.WebContext)
	q := c.QueryParam("q")
	result := showSearchResult{Query: q}

	showService, err := shows.NewShowService(ctx.Ctx)
	if err != nil {
		return err
	}

	if q != "" {
		showsResult, err := showService.SearchShows(q)
		if err != nil {
			return err
		}
		result.Shows = showsResult
	}

	return c.Render(http.StatusOK, "shows-search.html", result)
}

type showEdit struct {
	Show     shows.Show
	Episodes []shows.Episode
}

func ShowsEdit(c echo.Context) error {
	ctx := c.(*core.WebContext)

	show := shows.Show{}
	var episodes []shows.Episode
	id := ctx.ParamAsInt("id")

	showService, err := shows.NewShowService(ctx.Ctx)
	if err != nil {
		return err
	}

	if id > 0 {
		store := shows.NewStore(ctx.Ctx)
		var err error
		show, err = store.GetShow(int64(id))
		if err != nil {
			show, err = showService.LoadShow(id)
			if err != nil {
				return err
			}
		}

		episodes, err = store.LoadEpisodes(show.Id)
		if err != nil {
			log.Warnf("error loading epsiodes: %v", err)
		}
	}

	return c.Render(http.StatusOK, "show-edit.html", showEdit{Show: show, Episodes: episodes})
}

func ShowSave(c echo.Context) error {
	ctx := c.(*core.WebContext)
	store := shows.NewStore(ctx.Ctx)

	showService, err := shows.NewShowService(ctx.Ctx)
	if err != nil {
		return err
	}

	id := ctx.ParamAsInt("id")
	show, err := store.GetShow(int64(id))
	if err != nil {
		show, err = showService.LoadShow(id)
		if err != nil {
			return err
		}
	}
	show.Name = c.FormValue("name")
	show.SearchName = c.FormValue("searchName")
	show.Folder = c.FormValue("folder")

	if err := store.SaveShow(&show); err != nil {
		logrus.Errorf("error saving show: %v", err)
		return err
	}

	go func(ctx *core.Ctx, show *shows.Show) {
		if err := showService.UpdateShow(ctx, show); err != nil {
			log.Warnf("error while updating show: %v", err)
		}
	}(ctx.Ctx, &show)

	return ctx.Redirect(http.StatusFound, "/shows/list")
}

func ShowDelete(c echo.Context) error {
	ctx := c.(*core.WebContext)
	store := shows.NewStore(ctx.Ctx)

	id := ctx.ParamAsInt("id")
	show, err := store.GetShow(int64(id))
	if err != nil {
		return err
	}

	if err := store.DeleteShow(&show); err != nil {
		logrus.Errorf("error deleting show: %v", err)
		return err
	}

	return ctx.Redirect(http.StatusFound, "/shows/list")
}
