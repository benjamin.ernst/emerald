package views

import (
	"emerald/core"
	"emerald/irc"
	"emerald/log"
	"github.com/labstack/echo/v4"
	"net/http"
	"time"
)

type searchResult struct {
	Query   string
	Packets []irc.Packet
}

func Search(c echo.Context) error {
	ctx := c.(*core.WebContext).Ctx
	store := irc.NewStore(ctx)

	q := c.QueryParam("q")
	result := searchResult{Query: q}

	if q != "" {
		var err error
		startq := time.Now()
		result.Packets, err = store.QueryPackets(q)
		if err != nil {
			log.Warnf("error querying packets: %v", err)
		}
		log.Infof("query took: %v", time.Since(startq))
	}

	return c.Render(http.StatusOK, "search.html", result)
}

func StartDownload(c echo.Context) error {
	ctx := c.(*core.WebContext)
	store := irc.NewStore(ctx.Ctx)

	packet, err := store.GetPacket(c.Param("id"))
	if err != nil {
		log.Warnf("could not start download: %v", err)
		return err
	}

	client := ctx.IrcService.(*irc.Client)
	client.DownloadPacket(&packet)

	return ctx.Redirect(http.StatusFound, "/downloads")
}
