package views

import (
	"emerald/core"
	"emerald/files"
	"emerald/shows"
	"github.com/labstack/echo/v4"
	"net/http"
)

func Files(c echo.Context) error {
	ctx := c.(*core.WebContext).Ctx
	filesList := files.GetFiles(ctx)

	return c.Render(http.StatusOK, "files.html", filesList)
}

func Update(c echo.Context) error {
	ctx := c.(*core.WebContext)
	shows.ScanDownloadDir(ctx.Ctx)

	return ctx.Redirect(http.StatusFound, "/files")
}

func Move(c echo.Context) error {
	ctx := c.(*core.WebContext)

	dir := ctx.Param("dir")
	file := ctx.Param("file")

	if err := files.MoveFilesToMovies(ctx.Ctx, []files.File{{Folder: dir, Name: file}}); err != nil {
		return err
	}

	return ctx.Redirect(http.StatusFound, "/files")
}

func Delete(c echo.Context) error {
	ctx := c.(*core.WebContext)

	dir := ctx.Param("dir")
	file := ctx.Param("file")

	if err := files.DeleteFiles(ctx.Ctx, []files.File{{Folder: dir, Name: file}}); err != nil {
		return err
	}

	return ctx.Redirect(http.StatusFound, "/files")
}
