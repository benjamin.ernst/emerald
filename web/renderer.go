package web

import (
	"emerald/core"
	"fmt"
	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"html/template"
	"io"
	"os"
	"path/filepath"
	"strings"
)

const templatesDir = "web/tmpl"

type Template struct {
	templates map[string]*template.Template
}

func NewTemplate(ctx *core.Ctx) *Template {
	ext := ".html"
	ins := Template{
		templates: map[string]*template.Template{},
	}

	layout := templatesDir + "/base" + ext
	_, err := os.Stat(layout)
	if err != nil {
		logrus.Panicf("cannot find %s", layout)
		os.Exit(1)
	}

	funcMap := template.FuncMap{
		"basePath": func() string {
			return fmt.Sprintf("%s/", ctx.AppConfig.BasePath)
		},
	}

	views, _ := filepath.Glob(templatesDir + "**/*" + ext)

	// first find all partials
	partials := make([]string, 0)
	for _, view := range views {
		_, file := filepath.Split(view)
		if strings.HasPrefix(file, "_") {
			partials = append(partials, view)
		}
	}

	for _, view := range views {
		_, file := filepath.Split(view)
		//dir = strings.Replace(dir, templatesDir, "", 1)
		//file = strings.TrimSuffix(file, ext)
		renderName := file

		tmplFiles := append([]string{layout, view}, partials...)
		tmpl := template.Must(template.New(filepath.Base(layout)).Funcs(funcMap).ParseFiles(tmplFiles...))
		ins.Add(renderName, tmpl)
		logrus.Infof("renderName: %s, layout: %s", renderName, layout)

	}
	return &ins
}

func (t *Template) Add(name string, tmpl *template.Template) {
	if tmpl == nil {
		panic("template can not be nil")
	}
	if len(name) == 0 {
		panic("template name cannot be empty")
	}
	t.templates[name] = tmpl
}

func (t *Template) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	if _, ok := t.templates[name]; ok == false {
		return fmt.Errorf("no such view. (%s)", name)
	}
	return t.templates[name].Execute(w, data)
}
