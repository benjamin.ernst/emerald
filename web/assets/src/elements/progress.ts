export class Progress extends HTMLElement {
    stroke: number;
    radius: number;
    circumference: number;
    private root: ShadowRoot;

    connectedCallback() {
        this.stroke = parseInt(this.getAttribute('stroke')) || this.stroke;
        this.radius = parseInt(this.getAttribute('radius')) || this.radius;

        const normalizedRadius = this.radius - this.stroke * 2;
        this.circumference = normalizedRadius * 2 * Math.PI;

        this.root = this.attachShadow({mode: 'open'});
        this.root.innerHTML = `
      <div class="container">
          <svg height="${this.radius * 2}" width="${this.radius * 2}">
             <circle
               stroke="green"
               stroke-dasharray="${this.circumference} ${this.circumference}"
               style="stroke-dashoffset:${this.circumference}"
               stroke-width="${this.stroke}"
               fill="transparent"
               r="${normalizedRadius}"
               cx="${this.radius}"
               cy="${this.radius}"
            />
          </svg>
          <span class="label"></span>
      </div>
      <style>
        circle {
          transition: stroke-dashoffset 0.35s;
          transform: rotate(-90deg);
          transform-origin: 50% 50%;
        }
        .container {
          position: relative;
        }
        .label {
          text-align: center;
          font-size: 10px;
          position: absolute;
          top: ${this.radius - 7}px;
          left: 0;
          width: ${this.radius*2}px;
        }
      </style>
      
    `;

        const progress = this.getAttribute('progress');
        if (progress) {
            this.setProgress(progress);
        } else {
            const current = parseInt(this.getAttribute("current"));
            const total = parseInt(this.getAttribute("total"));
            this.setProgress((current / total) * 100);
        }
    }


    setProgress(percent) {
        percent = Math.round(percent);
        const offset = this.circumference - (percent / 100 * this.circumference);
        const circle = this.root.querySelector('circle');
        circle.style.strokeDashoffset = "" + offset;
        const label = this.root.querySelector('.label');
        label.innerHTML = percent + "%";
    }

    static get observedAttributes() {
        return ['progress', 'current', 'total'];
    }

    attributeChangedCallback(name, oldValue, newValue) {
        if (!this.root) return;
        if (name === 'progress') {
            this.setProgress(newValue);
        }
        if (name === 'current' || name === 'total') {
            const current = parseInt(this.getAttribute("current"));
            const total = parseInt(this.getAttribute("total"));
            this.setProgress((current / total) * 100);
        }
    }
}

customElements.define("progress-ring", Progress);