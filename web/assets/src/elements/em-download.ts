import {Progress} from "./progress";

export class EmDownload extends HTMLElement {
    private downloadId: string;

    constructor(){
        super();
    }

    connectedCallback() {
        this.addEventListener("click", this.handleClick.bind(this));
        this.downloadId = this.getAttribute("download-id");
        this.classList.add('hidden');

        window.addEventListener("em-toggle-download", this.onToggleEvent.bind(this));
    }

    disconnectedCallback() {
        window.removeEventListener("em-toggle-download", this.onToggleEvent.bind(this))
    }

    onToggleEvent(evt: CustomEvent) {
        if (evt.detail.id != this.downloadId) {
            this.classList.add('hidden');
        }
    }

    handleClick() {
        this.classList.toggle('hidden');
        window.dispatchEvent(new CustomEvent("em-toggle-download", {
            detail: {
                id: this.downloadId
            }
        }));
    }

    public getDownloadId():string {
        return this.downloadId;
    }

    updateStatus(data: any) {
        const dd = data.displayData;
        this.updateElement(".status", data.status);
        this.updateElement(".received", dd.bytesReceived);
        this.updateElement(".size", dd.size);
        this.updateElement(".speed", dd.speed);
        this.updateElement(".remaining", dd.remaining);

        const progress = <Progress>this.querySelector("progress-ring");
        if (progress) {
            progress.setProgress((data.bytesReceived / data.size) * 100)
        }
    }

    updateElement(selector:string, data:any) {
        const el = this.querySelector(selector);
        if (el) {
            el.innerHTML = data;
        }
    }
}
customElements.define("em-download", EmDownload);