import {EmDownload} from "./em-download";

class EmDownloadStatus extends HTMLElement {
    private ws: WebSocket;
    private downloads: Map<string, EmDownload>;

    constructor(){
        super();
    }

    connectedCallback() {
        const loc = window.location;
        let uri = 'ws:';

        if (loc.protocol === 'https:') {
            uri = 'wss:';
        }
        uri += '//' + loc.host;
        uri += loc.pathname + '/status';

        this.ws = new WebSocket(uri);

        this.ws.onopen = function() {
            console.log('Connected')
        };

        this.ws.onmessage = (evt) => {
            const items = JSON.parse(evt.data);
            this.updateStatus(items);
        }
    }

    updateStatus(items) {
        const downloads = this.getDownloads();
        if (downloads) {
            for (const item of items) {
                const download = downloads.get(item.id);
                if (download) {
                    download.updateStatus(item);
                }
            }
        }
    }

    getDownloads() {
        const downloads = <NodeListOf<EmDownload>>this.querySelectorAll("em-download");
        if (downloads && downloads.length > 0) {
            this.downloads = new Map<string, EmDownload>();
            for (let i = 0; i < downloads.length; i++) {
                const item = downloads[i];
                this.downloads.set(item.getDownloadId(), item);
            }


        }
        return this.downloads;
    }

    disconnectedCallback() {
        if (this.ws) {
            this.ws.close();
        }
    }

}
customElements.define("em-download-status", EmDownloadStatus);