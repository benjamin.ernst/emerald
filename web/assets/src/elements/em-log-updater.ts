class EmLogUpdater extends HTMLElement {
    private ws: WebSocket;

    constructor() {
        super();
    }

    connectedCallback() {
        const maxLogId = parseInt(this.getAttribute('max-log-id')) || 0;

        const loc = window.location;
        let uri = 'ws:';

        if (loc.protocol === 'https:') {
            uri = 'wss:';
        }
        uri += '//' + loc.host;
        uri += loc.pathname + '/update/' + maxLogId;

        this.ws = new WebSocket(uri);

        this.ws.onopen = function () {
            console.log('Connected logs-updater')
        };

        this.ws.onmessage = (evt) => {
            const items = JSON.parse(evt.data);
            if (items) {
                this.updateLogs(items);
            }
        }
    }

    updateLogs(items) {
        for (const item of items) {
            this.insertAdjacentHTML("afterbegin", `<div>
                <span>${this.formatDate(new Date(item.time))}</span>
                [<span class="level ${item.level}">${item.level}</span>]
                ${item.message}
            </div>`)
        }
    }


    formatDate(date: Date): string {
        const parts = new Intl.DateTimeFormat('en', {
            year: 'numeric',
            month: 'numeric',
            day: 'numeric',
            hour: 'numeric',
            hour12: false,
            minute: 'numeric',
            second: 'numeric'
        }).formatToParts(date);

        const year = parts.find((p) => p.type === 'year').value;
        const month = parts.find((p) => p.type === 'month').value;
        const day = parts.find((p) => p.type === 'day').value;
        const hour = parts.find((p) => p.type === 'hour').value;
        const minute = parts.find((p) => p.type === 'minute').value;
        const second = parts.find((p) => p.type === 'second').value;

        return `${year}-${month}-${day} ${hour}:${minute}:${second}`;
    }

    disconnectedCallback() {
        if (this.ws) {
            this.ws.close();
        }
    }

}

customElements.define("em-log-updater", EmLogUpdater);