class AddChannel extends HTMLElement {
    target: string;

    connectedCallback() {
        this.target = this.getAttribute('target') || this.target;
        this.addEventListener("click", this.handleClick.bind(this));
    }

    handleClick(e) {
        const channels = document.querySelector(this.target);
        if (channels) {
            const tmpl = document.getElementById('new-channel') as HTMLTemplateElement;
            channels.appendChild(tmpl.content.cloneNode(true));
        }
    }
}
customElements.define("add-channel", AddChannel);