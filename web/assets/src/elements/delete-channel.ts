class DeleteChannel extends HTMLElement {
    target: string;

    connectedCallback() {
        this.target = this.getAttribute('target') || this.target;
        this.addEventListener("click", this.handleClick.bind(this));
        this.innerHTML = `<svg>
                            <use xlink:href="assets/img/solid.svg#trash"></use>
                        </svg>`;
    }

    handleClick(e) {
        const channels = document.querySelector(this.target);
        if (channels) {
            channels.removeChild(this.parentNode);
        }
    }
}
customElements.define("delete-channel", DeleteChannel);