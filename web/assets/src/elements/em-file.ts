export class EmFile extends HTMLElement {
    private fileId: string;

    constructor(){
        super();
    }

    connectedCallback() {
        this.addEventListener("click", this.handleClick.bind(this));
        this.fileId = this.getAttribute("file-id");
        this.classList.add('hidden');

        window.addEventListener("em-toggle-file", this.onToggleEvent.bind(this));
    }

    disconnectedCallback() {
        window.removeEventListener("em-toggle-file", this.onToggleEvent.bind(this))
    }

    onToggleEvent(evt: CustomEvent) {
        if (evt.detail.id != this.fileId) {
            this.classList.add('hidden');
        }
    }

    handleClick() {
        this.classList.toggle('hidden');
        window.dispatchEvent(new CustomEvent("em-toggle-file", {
            detail: {
                id: this.fileId
            }
        }));
    }

    public getDownloadId():string {
        return this.fileId;
    }
}
customElements.define("em-file", EmFile);