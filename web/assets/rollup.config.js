import glob from 'glob';
import sass from 'rollup-plugin-sass';
import copy from 'rollup-plugin-copy';
import resolve from 'rollup-plugin-node-resolve';
import typescript from 'rollup-plugin-typescript';


function sassWatch() {
    return {
        name: 'sass-watch',
        transform(options) {
            const files = glob.sync("src/scss/**/*");
            for (const file of files) {
                this.addWatchFile(file);
            }
        }
    };
}

export default [
    {
        input: 'src/index.ts',
        output: {
            file: 'build/index.js',
            format: 'es',
            sourcemap: true
        },
        plugins: [
            copy({
                targets: [
                    {src: 'img/**/*', dest: 'build/img'},
                    {src: 'manifest.json', dest: 'build/'}
                ]
            }),
            sassWatch(),
            sass({
                // Filename to write all styles
                output: 'build/emerald.css',
            }),
            resolve(),
            typescript(),
        ]
    }
];