package irc

import (
	"emerald/log"
	"emerald/util"
	"errors"
	"fmt"
	irc "github.com/fluffle/goirc/client"
	"net"
	"sync"
	"time"
)

type dccConnection struct {
	net.Conn
	port   uint
	server net.Listener
}

type connectionHandler interface {
	createConnection(fileEvent *DccFileEvent, ircConnection *irc.Conn) (*dccConnection, error)
	closeConnection(conn *dccConnection) error
}

type dccConnectionHandler struct {
	ports map[uint]bool
	sync.Mutex
}

func newDccConnectionHandler(startPort uint, endPort uint) *dccConnectionHandler {
	freePorts := make(map[uint]bool)
	for port := startPort; port <= endPort; port++ {
		if port <= 0 {
			continue
		}
		freePorts[port] = true
	}

	return &dccConnectionHandler{
		ports: freePorts,
	}
}

func (dch *dccConnectionHandler) closeConnection(conn *dccConnection) error {
	err := conn.Close()

	if conn.port > 0 {
		_, exists := dch.ports[conn.port]
		if exists {
			dch.ports[conn.port] = true
		} else {
			err = errors.Join(err, fmt.Errorf("port %d of closed connection is not in port range", conn.port))
		}
	}

	if conn.server != nil {
		err = errors.Join(err, conn.server.Close())
	}

	return err
}

func (dch *dccConnectionHandler) createConnection(fileEvent *DccFileEvent, ircConnection *irc.Conn) (*dccConnection, error) {
	if fileEvent.Port == "0" {
		log.Infof("received passive send on port 0 for '%s'", fileEvent.FileName)

		return dch.startServerForPassiveDCC(fileEvent, ircConnection)
	}

	//format ip address
	ip := fileEvent.IP.String()
	if fileEvent.IP.To4() == nil {
		ip = fmt.Sprintf("[%s]", ip)
	}

	//connect
	log.Infof("connecting to %s:%s ...", ip, fileEvent.Port)
	tcpConn, err := net.Dial("tcp", fmt.Sprintf("%s:%s", ip, fileEvent.Port))
	if err != nil {
		log.Errorf("Connect error: %v", err)
		return nil, err
	}
	log.Infof("...connected to %s:%s", ip, fileEvent.Port)
	return &dccConnection{Conn: tcpConn}, nil
}

func (dch *dccConnectionHandler) startServerForPassiveDCC(event *DccFileEvent, ircConnection *irc.Conn) (*dccConnection, error) {
	port, err := dch.getFreePort()
	if err != nil {
		return nil, fmt.Errorf("error getting port for passive DCC: %w", err)
	}

	myIp, err := util.GetExternalIP()
	if err != nil {
		return nil, fmt.Errorf("error getting external IP for passive DCC: %w", err)
	}

	log.Infof("starting server for passive dcc on %s:%d ...", myIp.String(), port)

	server, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	if err != nil {
		return nil, fmt.Errorf("error creating server for passive DCC: %w", err)
	}

	newConns := make(chan net.Conn)
	go func(l net.Listener) {
		for {
			c, err := l.Accept()
			if err != nil {
				log.Errorf("error accepting connections on server for passive DCC: %v", err)
				newConns <- nil
				return
			}
			newConns <- c
			break
		}
	}(server)

	myIpAsInteger := util.IP2Int(myIp)
	msg := fmt.Sprintf("%s %d %d %d %s", event.FileName, myIpAsInteger, port, event.Size, event.Token)
	log.Infof("Sending Reverse to '%s': DCC SEND %s", event.Nick, msg)
	ircConnection.Ctcp(event.Nick, "DCC SEND", msg)

	for {
		select {
		case c := <-newConns:
			if c == nil {
				server.Close()
				dch.ports[port] = true
				return nil, fmt.Errorf("error accepting connections: got no connection")
			}

			log.Infof("client for '%s' is connected...", event.FileName)
			return &dccConnection{
				Conn:   c,
				port:   port,
				server: server,
			}, nil
		case <-time.After(time.Minute):
			server.Close()
			dch.ports[port] = true
			return nil, fmt.Errorf("timeout waiting for incomming connection")
		}
	}
}

func (dch *dccConnectionHandler) getFreePort() (uint, error) {
	dch.Lock()
	defer dch.Unlock()

	for port, available := range dch.ports {
		if available {
			// mark as unavailable
			dch.ports[port] = false
			return port, nil
		}
	}

	return 0, fmt.Errorf("could not find free port; all ports are in use")
}
