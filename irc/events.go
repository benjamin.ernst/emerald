package irc

import (
	"emerald/core"
	"emerald/log"
	"emerald/metrics"
	"time"
)

const maxPackets = 1000

func SubscribeNewPacket(ctx *core.Ctx) {
	store := NewStore(ctx)
	incoming := core.SubscribeEvent(core.NewPacket, 5000)

	packets := make([]*Packet, maxPackets)
	index := 0

	//start update ticker
	updateTicker := time.NewTicker(1 * time.Minute)

	savePackets := func() {
		if err := store.AddPackets(packets, index); err != nil {
			log.Errorf("error adding packets: %v", err)
		}
		packets = make([]*Packet, maxPackets)
		index = 0
	}

	go func() {
		for {
			select {
			case <-updateTicker.C:
				metrics.AddValue("NewPacketBuffer", float32(len(incoming)))
				savePackets()
				continue

			case e := <-incoming:
				packet := e.Data.(*Packet)
				packets[index] = packet
				index++
				if index >= maxPackets {
					savePackets()
				}
				continue
			}
		}
	}()
	log.Infof("listening to '%s' events...", core.NewPacket)
}
