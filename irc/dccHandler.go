package irc

import (
	"emerald/core"
	"emerald/log"
	"emerald/util"
	irc "github.com/fluffle/goirc/client"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

// HandleDCC handles all incomming DCC requests
func (ib *IrcBot) handleDCC(c *core.Ctx) irc.HandlerFunc {
	return func(conn *irc.Conn, line *irc.Line) {
		log.Infof("received DCC command: %v", line.Raw)
		request := strings.Split(line.Args[2], " ")
		ctcpType := line.Args[0]
		if ctcpType == "DCC" {
			cmd := request[0]
			if cmd == "SEND" {
				ib.handleSend(c, request, conn, line)
			} else if cmd == "ACCEPT" {
				ib.handleAccept(c, request)
			} else {
				log.Infof("received unmatched DCC command: %v", cmd)
			}
		} else if ctcpType == "VERSION" {
			ib.conn.CtcpReply(line.Nick, "VERSION", "emerald-new", c.AppConfig.BuildVersion)
		} else {
			log.Infof("received unmatched ctcp command: %v", line.Text())
		}
	}
}

func (ib *IrcBot) handleSend(c *core.Ctx, request []string, conn *irc.Conn, line *irc.Line) {
	fileName := request[1]
	address := util.Int2IP(request[2])
	port := request[3]
	size, _ := strconv.ParseInt(request[4], 0, 64)

	token := ""
	// token in only available for passive DCC
	if len(request) >= 6 {
		token = request[5]
	}

	log.Infof("received SEND - nick: %v file: %v, addr: %v, port: %v, size: %v, token: %v", line.Nick, fileName, address.String(), port, size, token)

	download, exists := ib.getPending(line.Nick, fileName)
	if !exists {
		log.Errorf("could not find download for file-event %s %s", line.Nick, fileName)
		return
	}

	fileEvent := DccFileEvent{download.ID, line.Nick, "SEND", fileName, address, port, size, token}

	exists, startPos := fileExists(c, &fileEvent)

	if exists && startPos > 0 {
		// file already exists -> send resume request
		msg := fileName + " " + port + " " + strconv.FormatInt(startPos, 10)
		log.Infof("sending resume [%v]", msg)
		conn.Ctcp(line.Nick, "DCC RESUME", msg)
		//add to resumes
		ib.resumes[fileEvent.FileName] = &fileEvent
	} else {

		// This is a new file start from beginning
		go ib.startDownload(c, &fileEvent, startPos)
	}
}

func (ib *IrcBot) handleAccept(c *core.Ctx, request []string) {
	fileName := request[1]
	position, err := strconv.ParseInt(request[3], 10, 64)
	if err != nil {
		log.Infof("error while parsing position %v", err)
		return
	}
	log.Infof("received ACCEPT (Resume) - file: %v, position: %v\n", fileName, position)

	//find resume
	fileEvent, exists := ib.resumes[fileName]
	if !exists {
		log.Infof("can not find resume for %v", fileName)
		return
	}
	delete(ib.resumes, fileName)

	//start the download in new goroutine
	go ib.startDownload(c, fileEvent, position)
}

func (ib *IrcBot) startDownload(c *core.Ctx, fileEvent *DccFileEvent, startPos int64) {
	conn, err := ib.client.connHandler.createConnection(fileEvent, ib.conn)
	if err != nil {
		log.Errorf("could not create connection: %v", err)
		return
	}
	defer func() {
		if err := ib.client.connHandler.closeConnection(conn); err != nil {
			log.Warnf("error closing connection: %v", err)
		}
	}()

	downloader := NewDownloader(conn, c.Settings().TempDir, c.Settings().DownloadDir)
	defer downloader.Close()

	progress := downloader.GetProgress()
	go func() {
		for totalBytes := range progress {
			ib.client.updateChan <- DccUpdate{DownloadId: fileEvent.DownloadId, TotalBytes: totalBytes, Size: fileEvent.Size, Type: UPDATE}
		}
	}()

	totalBytes, err := downloader.Load(fileEvent.FileName, startPos)
	if err != nil {
		log.Errorf("error downloading file %s: %v", fileEvent.FileName, err)
		ib.client.updateChan <- DccUpdate{DownloadId: fileEvent.DownloadId, TotalBytes: totalBytes, Size: fileEvent.Size, Type: FAIL}
		return
	}

	ib.client.updateChan <- DccUpdate{DownloadId: fileEvent.DownloadId, TotalBytes: totalBytes, Size: fileEvent.Size, Type: COMPLETE}
}

func fileExists(c *core.Ctx, fileEvent *DccFileEvent) (bool, int64) {
	filename := filepath.FromSlash(c.Settings().TempDir + "/" + fileEvent.FileName)
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false, 0
	}
	return true, info.Size()
}
