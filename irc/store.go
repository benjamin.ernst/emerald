package irc

import (
	"emerald/core"
	"emerald/log"
	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
	"strings"
	"time"
)

type Store struct {
	db *sqlx.DB
}

func NewStore(c *core.Ctx) *Store {
	return &Store{db: c.Db}
}

func (ps *Store) CreateServer(server *Server) error {
	tx, err := ps.db.Beginx()
	if err != nil {
		return err
	}

	rs, err := tx.NamedExec("INSERT INTO servers (name, port, ssl) VALUES(:name, :port, :ssl)", server)
	if err != nil {
		return err
	}
	id, err := rs.LastInsertId()
	if err != nil {
		return err
	}
	server.Id = int(id)

	err = ps.updateChannels(tx, server)
	if err != nil {
		return err
	}

	return tx.Commit()
}

func (ps *Store) UpdateServer(server *Server) error {
	tx, err := ps.db.Beginx()
	if err != nil {
		return err
	}

	_, err = tx.NamedExec("UPDATE servers SET name=:name, port=:port, ssl=:ssl WHERE id=:id", server)
	if err != nil {
		return err
	}

	err = ps.updateChannels(tx, server)
	if err != nil {
		return err
	}

	return tx.Commit()
}

func (ps *Store) GetServer(id int) (*Server, error) {
	var server Server

	if err := ps.db.Get(&server, "SELECT * FROM servers WHERE id=$1", id); err != nil {
		return nil, err
	}

	channels, err := ps.loadChannels(id)
	if err != nil {
		return nil, err
	}
	server.Channels = channels

	return &server, nil
}

func (ps *Store) GetServers() ([]*Server, error) {
	var servers []*Server

	if err := ps.db.Select(&servers, "SELECT * FROM servers"); err != nil {
		return servers, err
	}

	for _, server := range servers {
		channels, err := ps.loadChannels(server.Id)
		if err != nil {
			return servers, err
		}
		server.Channels = channels
	}

	return servers, nil
}

func (ps *Store) DeleteServer(server *Server) error {
	tx, err := ps.db.Beginx()
	if err != nil {
		return err
	}

	_, err = tx.Exec("DELETE FROM servers WHERE id=$1", server.Id)
	if err != nil {
		return err
	}

	err = ps.deleteChannels(tx, server.Id)
	if err != nil {
		return err
	}

	return tx.Commit()
}

func (ps *Store) updateChannels(tx *sqlx.Tx, server *Server) error {
	err := ps.deleteChannels(tx, server.Id)
	if err != nil {
		return err
	}
	for _, channel := range server.Channels {
		if _, err := tx.Exec("INSERT INTO channels (name, server_id) VALUES($1, $2)", channel.Name, server.Id); err != nil {
			return err
		}
	}
	return nil
}

func (ps *Store) deleteChannels(tx *sqlx.Tx, serverId int) error {
	if _, err := tx.Exec("DELETE FROM channels where server_id = $1", serverId); err != nil {
		return err
	}
	return nil
}

func (ps *Store) loadChannels(serverId int) ([]Channel, error) {
	var channels []Channel
	err := ps.db.Select(&channels, "SELECT name FROM channels WHERE server_id=$1", serverId)
	return channels, err
}

// PACKETS

const packetUpsert = `INSERT INTO packets 
(id, packet_id, size, name, bot, channel, server, date) 
VALUES (:id, :packet_id, :size, :name, :bot, :channel, :server, :date)
ON CONFLICT (id)
DO UPDATE SET packet_id=:packet_id, size=:size, name=:name, bot=:bot, channel=:channel, server=:server, date=:date
WHERE id = :id
`

func (ps *Store) AddPacket(packet *Packet) error {
	_, err := ps.db.NamedExec(packetUpsert, packet)
	return err
}

func (ps *Store) AddPackets(packets []*Packet, size int) error {
	if size <= 0 || packets == nil || len(packets) <= 0 {
		return nil
	}

	start := time.Now()
	tx, err := ps.db.Beginx()
	if err != nil {
		return err
	}

	for i := 0; i < size; i++ {
		packet := packets[i]
		_, err := tx.NamedExec(packetUpsert, packet)
		if err != nil {
			return err
		}
	}

	if err := tx.Commit(); err != nil {
		return err
	}
	log.Infof("adding %d packets took: %s", size, time.Since(start))
	return nil
}

func (ps *Store) QueryPackets(queryString string) ([]Packet, error) {
	parts := strings.Split(queryString, " ")
	query := "%" + strings.Join(parts, "%") + "%"

	var packets []Packet
	if err := ps.db.Select(&packets, "SELECT * FROM packets WHERE name like $1 order by date desc", query); err != nil {
		return nil, err
	}

	return packets, nil
}

func (ps *Store) CountPackets() (int, error) {
	var count int
	err := ps.db.Get(&count, "SELECT count(*) FROM packets")
	return count, err
}

func (ps *Store) DeleteOldPackets() error {
	minusOneDay, _ := time.ParseDuration("-12h")
	yesterday := time.Now().Add(minusOneDay)

	rs, err := ps.db.Exec("DELETE from packets where date < $1", yesterday)
	if err != nil {
		return err
	}
	count, err := rs.RowsAffected()
	if err != nil {
		return err
	}
	logrus.Infof("deleted %d old packets", count)
	return nil
}

func (ps *Store) GetPacket(id string) (Packet, error) {
	var packet Packet
	if err := ps.db.Get(&packet, "SELECT * FROM packets WHERE id=$1", id); err != nil {
		return packet, err
	}
	return packet, nil
}
