package irc

import (
	"crypto/tls"
	"emerald/core"
	"emerald/log"
	"fmt"
	"net"
	"regexp"
	"strings"
	"sync"
	"time"
	"unicode"

	irc "github.com/fluffle/goirc/client"
)

var packetRegex = regexp.MustCompile(`(#[0-9]+).*\[\s*([0-9|\.]+[BbGgiKMs]+)\]\s+(.+).*`)

// IrcBot can connect to a specific server
type IrcBot struct {
	client           *Client
	host             string
	port             string
	ssl              bool
	channels         []string
	conn             *irc.Conn
	pending          map[string]*Download
	resumes          map[string]*DccFileEvent
	connMu           sync.Mutex
	reconnectAllowed bool
}

// NewIrcBot creates a new bot
func NewIrcBot(client *Client, server *Server) *IrcBot {
	bot := &IrcBot{client: client, host: server.Name, port: server.Port, ssl: server.Ssl}
	bot.channels = make([]string, len(server.Channels))
	for i, channel := range server.Channels {
		bot.channels[i] = channel.Name
	}

	//Initialize internal stuff
	bot.pending = make(map[string]*Download)
	bot.resumes = make(map[string]*DccFileEvent)
	return bot
}

// IsConnected checks whether a bot is connected to its server
func (ib *IrcBot) IsConnected() bool {
	if ib.conn == nil {
		return false
	}
	return ib.conn.Connected()
}

// Connect connects the bot to its serve
func (ib *IrcBot) Connect(c *core.Ctx) {
	ib.connMu.Lock()
	defer ib.connMu.Unlock()

	//reconnect
	ib.reconnectAllowed = true

	settings := c.Settings()
	cfg := irc.NewConfig(settings.Nick)
	cfg.Timeout = 10 * time.Second

	if ib.ssl {
		cfg.SSL = true
		cfg.SSLConfig = &tls.Config{InsecureSkipVerify: true}
	}

	ib.conn = irc.Client(cfg)

	// Join channels
	ib.conn.HandleFunc(irc.CONNECTED,
		func(conn *irc.Conn, line *irc.Line) {
			log.Infof("connected to %s: %s", ib.host, ib.port)

			for _, channel := range ib.channels {
				log.Infof("joining channel %s", channel)
				conn.Join(channel)
			}
		})

	// Parse Messages
	ib.conn.HandleFunc(irc.PRIVMSG, ib.parseMessage)

	ib.conn.HandleFunc("372", ib.log372)

	ib.conn.HandleFunc(irc.DISCONNECTED, ib.handleDisconnect(c))

	ib.conn.HandleFunc(irc.CTCP, ib.handleDCC(c))

	ib.conn.HandleFunc(irc.NOTICE, ib.handleNotice)

	// Tell client to connect.
	ips, err := net.LookupIP(ib.host)
	if err != nil {
		log.Errorf("Lookup error: %v", err)
	}

	hosts := make([]string, len(ips)+1)
	hosts[0] = fmt.Sprintf("%s:%s", ib.host, ib.port)
	for i, ip := range ips {
		hosts[i+1] = fmt.Sprintf("%s:%s", ip.String(), ib.port)
	}

	for _, host := range hosts {
		log.Infof("Connecting to '%v'", host)
		if err := ib.conn.ConnectTo(host); err != nil {
			log.Errorf("Connection error: %v", err)
		} else {
			break
		}
	}
}

// Disconnect disconnects the bot from its server. Currently NOOP
func (ib *IrcBot) Disconnect() {
	log.Infof("Disconnecting from '%v'.", ib.host)
	ib.reconnectAllowed = false
	ib.conn.Quit()
}

func (ib *IrcBot) handleDisconnect(c *core.Ctx) irc.HandlerFunc {
	return func(conn *irc.Conn, line *irc.Line) {
		log.Infof("Disconnected from '%v'.", ib.host)
		if ib.reconnectAllowed {
			log.Infof(" --> Reconnecting now ...")
			ib.Connect(c)
		}
	}
}

func (ib *IrcBot) log372(conn *irc.Conn, line *irc.Line) {
	log.Infof(line.Text())
}

func (ib *IrcBot) handleNotice(conn *irc.Conn, line *irc.Line) {
	log.Infof("[NOTICE: %s] %s", line.Nick, line.Text())
}

func (ib *IrcBot) parseMessage(conn *irc.Conn, line *irc.Line) {
	packet := ib.parsePacket(line)
	if packet != nil {
		//save packet
		core.Publish(core.NewPacket, packet)
	}
}

func (ib *IrcBot) parsePacket(line *irc.Line) *Packet {
	result := packetRegex.FindStringSubmatch(line.Text())
	if result == nil {
		return nil
	}

	packetId := result[1]
	size := result[2]
	fileName := cleanFileName(result[3])
	bot := line.Nick
	channel := line.Target()

	packet := &Packet{
		Id:       channel + ":" + bot + ":" + packetId,
		PacketId: packetId,
		Size:     size,
		Name:     fileName,
		Bot:      bot,
		Channel:  channel,
		Server:   ib.host,
		Date:     line.Time,
	}

	return packet
}

func cleanFileName(filename string) string {
	filename = strings.Trim(filename, "\u0003\u263B\u263C\u0002\u000f\u0016\u001f ")

	return removeDigitsFromStringStart(filename)
}

func removeDigitsFromStringStart(s string) string {
	// Convert string to rune slice to handle Unicode characters
	runes := []rune(s)

	// Iterate through the string to find the index where digits end
	var index int
	for index = 0; index < len(runes); index++ {
		if !unicode.IsDigit(runes[index]) {
			break
		}
	}

	// Return the substring after the digits
	return string(runes[index:])
}

// StartDownload starts the given download
func (ib *IrcBot) StartDownload(download *Download) {
	log.Infof("Starting Download: %s", download.File)

	//add to pending list
	ib.addPending(download)

	msg := fmt.Sprintf("xdcc send %s", download.PacketID)
	ib.conn.Privmsg(download.Bot, msg)
	log.Infof("started download bot '%s-%s': %s", ib.host, download.Bot, msg)
}

// StopDownload stops the given download
func (ib *IrcBot) StopDownload(download *Download) {
	log.Infof("Stopping Download: %s", download.File)

	msg := "XDCC CANCEL"
	ib.conn.Privmsg(download.Bot, msg)
}

func (ib *IrcBot) addPending(download *Download) {
	ib.pending[createUniqueDccFileId(download.Bot, download.File)] = download
}

func (ib *IrcBot) getPending(bot string, file string) (*Download, bool) {
	dl, ok := ib.pending[createUniqueDccFileId(bot, file)]
	return dl, ok
}

func createUniqueDccFileId(bot string, file string) string {
	return fmt.Sprintf("%s_%s", bot, file)
}
