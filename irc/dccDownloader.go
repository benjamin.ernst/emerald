package irc

import (
	"bufio"
	"emerald/log"
	"encoding/binary"
	"fmt"
	"io"
	"net"
	"os"
	"path/filepath"
	"time"
)

type Messanger interface {
	Ctcp(t, ctcp string, arg ...string)
}

type Downloader struct {
	conn            net.Conn
	tempDir         string
	targetDir       string
	progressChannel chan int64
}

func NewDownloader(conn net.Conn, tempDir string, targetDir string) *Downloader {
	return &Downloader{
		conn:            conn,
		tempDir:         tempDir,
		targetDir:       targetDir,
		progressChannel: make(chan int64, 50),
	}
}

func (dl *Downloader) Close() {
	close(dl.progressChannel)
}

func (dl *Downloader) GetProgress() chan int64 {
	return dl.progressChannel
}

func (dl *Downloader) Load(filename string, startPos int64) (int64, error) {
	file := getTempFile(dl.tempDir, filename)

	// set start position
	var totalBytes int64
	totalBytes = startPos
	_, err := file.Seek(startPos, 0)
	if err != nil {
		return 0, err
	}

	// make a write buffer
	w := bufio.NewWriter(file)

	// close file on exit and check for its returned error
	defer func() {
		if err := file.Close(); err != nil {
			log.Errorf("error closing file: %v", err)
		}
	}()

	totalBytes, err = dl.downloadFile(totalBytes, w)
	if err != nil {
		return 0, err
	}

	// move
	if err := moveCompletedFile(filename, dl.tempDir, dl.targetDir); err != nil {
		return 0, err
	}

	return totalBytes, nil
}

func (dl *Downloader) downloadFile(totalBytes int64, w *bufio.Writer) (int64, error) {
	timeoutDuration := 5 * time.Second
	timeoutCount := 0
	var inBuf = make([]byte, 2048)
	counter := 0

	ackChan := make(chan dccAck, 50)
	defer close(ackChan)
	go sendAcknowledgements(ackChan, dl.conn)

	//read-loop
	for {
		// Set a deadline for reading. Read operation will fail if no data
		// is received after deadline.
		if err := dl.conn.SetReadDeadline(time.Now().Add(timeoutDuration)); err != nil {
			log.Errorf("could not set read-deadline: %v", err)
		}

		//read a chunk
		n, err := dl.conn.Read(inBuf)
		if err != nil {
			if err == io.EOF {
				// all done
				return totalBytes, nil
			} else if netErr, ok := err.(net.Error); ok && netErr.Timeout() {
				timeoutCount++
				if timeoutCount < 5 {
					dl.writeToAck(ackChan, totalBytes)
					counter = 0
					continue
				}

				return totalBytes, fmt.Errorf("timeout error: %s", err)
			} else {
				return totalBytes, fmt.Errorf("read error: %s", err)
			}
		}
		totalBytes += int64(n)

		// write to File
		if _, err := w.Write(inBuf[:n]); err != nil {
			return totalBytes, fmt.Errorf("write to file error: %s", err)
		}
		if err = w.Flush(); err != nil {
			return totalBytes, fmt.Errorf("flush error: %s", err)
		}

		// only send updates every 200 KB
		counter++
		if counter == 100 {
			dl.writeToAck(ackChan, totalBytes)
			counter = 0
		}
	}
}

func (dl *Downloader) writeToAck(ackChan chan dccAck, totalBytes int64) {
	ackChan <- dccAck{totalBytes: totalBytes}
	dl.progressChannel <- totalBytes
}

// DccUpdate -
type dccAck struct {
	totalBytes int64
}

// Send back an acknowledgement of how many bytes we have got so far.
func sendAcknowledgements(ackChan chan dccAck, conn net.Conn) {
	for {
		ack, ok := <-ackChan
		if !ok {
			log.Infof("closing ack-channel")
			break
		}
		//Convert bytesTransfered to an "unsigned, 4 byte integer in network byte order", per DCC specification
		outBuf := makeOutBuffer(ack.totalBytes)
		if n, err := conn.Write(outBuf); err != nil {
			log.Errorf("Write error (written %d bytes): %v", n, err)
		}
	}
}

func makeOutBuffer(totalBytes int64) []byte {
	var bytes = make([]byte, 4)
	binary.BigEndian.PutUint32(bytes, uint32(totalBytes))
	return bytes
}

func getTempFile(tempDir string, filename string) *os.File {
	filename = filepath.Join(tempDir, filename)
	_, err := os.Stat(filename)

	if os.IsNotExist(err) {
		fo, err := os.Create(filename)
		if err != nil {
			log.Errorf("File create error: %s", err)
		}
		return fo
	}

	fo, err := os.OpenFile(filename, os.O_WRONLY, 0777)
	if err != nil {
		log.Errorf("File open error: %s", err)
	}
	return fo
}

func moveCompletedFile(filename string, tempDir string, targetDir string) error {
	srcFile := filepath.Join(tempDir, filename)
	dstFile := filepath.Join(targetDir, filename)
	err := os.Rename(srcFile, dstFile)
	if err != nil {
		return fmt.Errorf("Error while moving file to destination: %w", err)
	}

	return nil
}
