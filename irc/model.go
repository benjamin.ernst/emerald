package irc

import (
	"github.com/dustin/go-humanize"
	"github.com/google/uuid"
	"net"
	"net/url"
	"time"
)

// DccFileEvent -
type DccFileEvent struct {
	DownloadId string
	Nick       string
	Type       string
	FileName   string
	IP         net.IP
	Port       string
	Size       int64
	Token      string
}

// UpdateType -
type UpdateType int

// Update constants
const (
	UPDATE   UpdateType = 0
	COMPLETE UpdateType = 1
	FAIL     UpdateType = 2
)

// DccUpdate -
type DccUpdate struct {
	DownloadId string
	TotalBytes int64
	Size       int64
	Type       UpdateType
}

// Download represent a Download in Emerald
type Download struct {
	ID            string      `json:"id"`
	Status        string      `json:"status"`
	File          string      `json:"file"`
	PacketID      string      `json:"packetId"`
	Server        string      `json:"server"`
	Bot           string      `json:"bot"`
	BytesReceived int64       `json:"bytesReceived"`
	Size          int64       `json:"size"`
	Speed         float32     `json:"speed"`
	Remaining     int64       `json:"remaining"`
	LastUpdate    time.Time   `json:"lastUpdate"`
	DisplayData   DisplayData `json:"displayData"`
}

func NewDownload(packet *Packet) *Download {
	return &Download{
		ID:         uuid.New().String(),
		Status:     "WAITING",
		File:       packet.Name,
		PacketID:   packet.PacketId,
		Server:     packet.Server,
		Bot:        packet.Bot,
		LastUpdate: time.Now(),
	}
}

func (d *Download) FillDisplayData() {
	d.DisplayData = DisplayData{
		BytesReceived: humanize.Bytes(uint64(d.BytesReceived)),
		Size:          humanize.Bytes(uint64(d.Size)),
		Speed:         humanize.Bytes(uint64(d.Speed)),
		Remaining:     (time.Duration(d.Remaining) * time.Second).String(),
	}
}

type DisplayData struct {
	BytesReceived string `json:"bytesReceived"`
	Size          string `json:"size"`
	Speed         string `json:"speed"`
	Remaining     string `json:"remaining"`
}

// Server represents a irc-server
type Server struct {
	Id       int       `json:"id"       db:"id"`
	Name     string    `json:"name"     db:"name"`
	Port     string    `json:"port"     db:"port"`
	Ssl      bool      `json:"ssl"      db:"ssl"`
	Channels []Channel `json:"channels" db:"-"`
}

type Channel struct {
	Name string `json:"name" db:"name"`
}

// Packet
type Packet struct {
	Id       string    `json:"id"        db:"id"`
	PacketId string    `json:"packetId"  db:"packet_id"`
	Size     string    `json:"size"      db:"size"`
	Name     string    `json:"name"      db:"name"`
	Bot      string    `json:"bot"       db:"bot"`
	Channel  string    `json:"channel"   db:"channel"`
	Server   string    `json:"server"    db:"server"`
	Date     time.Time `json:"date"      db:"date"`
}

// SaveId returns an url-save ID. Used in templates
func (p *Packet) SaveId() string {
	return url.PathEscape(p.Id)
}
