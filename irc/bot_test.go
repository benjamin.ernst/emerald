package irc

import (
	"testing"
)

func TestCleanFilename(t *testing.T) {
	filename := "☻Beckoning.The.Butcher.2013.DVDRiP.X264-TASTE.mkv☼"
	result := cleanFileName(filename)

	if result != "Beckoning.The.Butcher.2013.DVDRiP.X264-TASTE.mkv" {
		t.Error("wrong filename: " + result)
	}
}
