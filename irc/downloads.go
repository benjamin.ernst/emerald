package irc

import (
	"emerald/core"
	"emerald/log"
	"fmt"
	"time"
)

// DownloadPacket starts the Download of a Packet
func (ic *Client) DownloadPacket(packet *Packet) {
	bot, exists := ic.GetBot(packet.Server)
	if exists && bot.IsConnected() {
		download := NewDownload(packet)
		ic.AddNewDownload(download)
		bot.StartDownload(download)
	}
}

// AddNewDownload add a new Download to the list
func (ic *Client) AddNewDownload(download *Download) {
	_, exists := ic.downloads[download.ID]
	if !exists {
		ic.downloads[download.ID] = download
	} else {
		log.Infof("Received new Dowload '%v' that already existed", download.ID)
	}
}

// ListDownloads returns a slice of all Downloads
func (ic *Client) ListDownloads() []*Download {
	v := make([]*Download, 0, len(ic.downloads))

	for _, value := range ic.downloads {
		v = append(v, value)
	}
	return v
}

func (ic *Client) GetDownload(id string) (*Download, error) {
	dl, ok := ic.downloads[id]
	if !ok {
		return nil, fmt.Errorf("could not find download for id %s", id)
	}

	return dl, nil
}

// StopDownload stops the given download
func (ic *Client) StopDownload(download *Download) {
	bot, exists := ic.GetBot(download.Server)
	if exists {
		bot.StopDownload(download)
	}
}

// CancelDownload cancels the given download
func (ic *Client) CancelDownload(downloadId string) {
	download, err := ic.GetDownload(downloadId)
	if err == nil {
		if download.Status == "RUNNING" {
			ic.StopDownload(download)
		}
		delete(ic.downloads, download.ID)
	}
}

// ResumeDownload resumes the given download
func (ic *Client) ResumeDownload(parsedDownload *Download) {
	download, err := ic.GetDownload(parsedDownload.ID)
	if err != nil {
		log.Warnf("could not resume download: %v", err)
		return
	}

	if download.Status != "RUNNING" {
		bot, exists := ic.GetBot(download.Server)
		if exists {
			bot.StartDownload(download)
		}
	}
}

func (ic *Client) updateDownloads() {
	for {
		update := <-ic.updateChan
		switch update.Type {
		case UPDATE:
			ic.updateDownload(update)
		case COMPLETE:
			ic.completeDownload(update)
		case FAIL:
			ic.failDownload(update)
		}
	}
}

func (ic *Client) updateDownload(update DccUpdate) {
	download, err := ic.GetDownload(update.DownloadId)
	if err != nil {
		log.Warnf("could not update download: %v", err)
		return
	}

	//calc speed
	now := time.Now()
	sizeDelta := (update.TotalBytes - download.BytesReceived) / 1024
	timeDelta := now.UnixNano() - download.LastUpdate.UnixNano()
	download.Speed = (float32(sizeDelta) / float32(timeDelta)) * 1000 * 1000 * 1000 * 1000

	//update download
	download.LastUpdate = now
	download.Status = "RUNNING"
	download.BytesReceived = update.TotalBytes
	download.Size = update.Size
	download.Remaining = int64(float32(download.Size-download.BytesReceived) / download.Speed)
}

func (ic *Client) completeDownload(update DccUpdate) {
	download, err := ic.GetDownload(update.DownloadId)
	if err != nil {
		log.Warnf("could not complete download: %v", err)
		return
	}

	download.Status = "COMPLETE"
	log.Infof("Download completed '%v'", download.File)

	core.Publish(core.DownloadComplete, download.File)
}

func (ic *Client) failDownload(update DccUpdate) {
	download, err := ic.GetDownload(update.DownloadId)
	if err != nil {
		log.Warnf("could not fail download: %v", err)
		return
	}

	log.Infof("Download failed '%v'", download.File)
	download.Status = "FAILED"
}
