package irc

import (
	"emerald/util"
	"net"
	"reflect"
	"testing"
)

func Test_int2ip(t *testing.T) {
	tests := []struct {
		name string
		ip   string
		want net.IP
	}{
		{
			"1", "1509217306", net.ParseIP("89.244.212.26"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := util.Int2IP(tt.ip); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("int2ip() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_ip2int(t *testing.T) {
	tests := []struct {
		name string
		ip   net.IP
		want uint32
	}{
		{
			"1", net.ParseIP("89.244.212.26"), 1509217306,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := util.IP2Int(tt.ip); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("int2ip() = %v, want %v", got, tt.want)
			}
		})
	}
}
