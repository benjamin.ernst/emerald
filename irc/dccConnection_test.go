package irc

import (
	"fmt"
	"net"
	"testing"
)

func Test_Connection(t *testing.T) {
	conn, err := net.Dial("tcp", "87.123.27.215:17459")
	if err != nil {
		fmt.Println("Error:", err)
		return
	}
	defer conn.Close()
	fmt.Println("Connected")
}
