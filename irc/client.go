package irc

import (
	"emerald/core"
)

// Client is a irc client that cann connect to multiple irc servers
type Client struct {
	bots        map[string]*IrcBot
	downloads   map[string]*Download
	updateChan  chan DccUpdate
	connHandler connectionHandler
}

// NewClient creates a new client
func NewClient(ctx *core.Ctx) *Client {
	client := new(Client)
	client.bots = make(map[string]*IrcBot)
	client.downloads = make(map[string]*Download)
	client.updateChan = make(chan DccUpdate)

	settings := ctx.Settings()
	client.connHandler = newDccConnectionHandler(uint(settings.StartPort), uint(settings.EndPort))

	//start download updates
	go client.updateDownloads()

	return client
}

func (ic *Client) Connect(c *core.Ctx) {
	for _, bot := range ic.bots {
		if !bot.IsConnected() {
			bot.Connect(c)
		}
	}
}

func (ic *Client) Disconnect(c *core.Ctx) {
	for _, bot := range ic.bots {
		if bot.IsConnected() {
			bot.Disconnect()
		}
	}
}

// ToggleConnection connects or disconnects the client
func (ic *Client) ToggleConnection(c *core.Ctx, server *Server) {
	bot := ic.getAndUpdateBot(server)
	if bot.IsConnected() {
		bot.Disconnect()
		delete(ic.bots, server.Name)
	} else {
		bot.Connect(c)
	}
}

func (ic *Client) ConnectServer(c *core.Ctx, server *Server) {
	bot := ic.getAndUpdateBot(server)
	if !bot.IsConnected() {
		bot.Connect(c)
	}
}

func (ic *Client) DisConnectServer(c *core.Ctx, server *Server) {
	bot := ic.getAndUpdateBot(server)
	if bot.IsConnected() {
		bot.Disconnect()
		delete(ic.bots, server.Name)
	}
}

// IsServerConnected returns true if the given server is connected
func (ic *Client) IsServerConnected(server *Server) bool {
	bot := ic.getAndUpdateBot(server)
	return bot.IsConnected()
}

// GetBot return the Bot of the given serverName
func (ic *Client) GetBot(serverName string) (*IrcBot, bool) {
	bot, exists := ic.bots[serverName]
	return bot, exists
}

func (ic *Client) getAndUpdateBot(server *Server) *IrcBot {
	currentBot, ok := ic.bots[server.Name]
	if !ok {
		// create new bot
		currentBot = NewIrcBot(ic, server)
		ic.bots[server.Name] = currentBot
	}
	return currentBot
}
