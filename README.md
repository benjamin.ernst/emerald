# Emerald

Find the best Gems in DCC

## Running Emerald

### Environment-Variables

Emerald is configured via environment variables.

| Value        | Description                                                                                      | default-value    |
|--------------|--------------------------------------------------------------------------------------------------|------------------|
| PORT         | The port where emerald is running                                                                | `8080`           |
| BASE_PATH    | The path under which emerald is listening                                                        | `/emerald`       |
| DB_FILE      | The location internal database                                                                   | `emerald.sqlite` |
| TMDB_API_KEY | The API-Key for the [TMDB-API](https://developer.themoviedb.org/reference/intro/getting-started) | `emerald.sqlite` |

run the executable with the flag "conf" indication the location of the config-file:

**For Linux:**

```shell script
PORT=8090 BASE_PATH=/my/path DB_FILE=/mnt/data/emerald.sqlite emerald 
```

## Docker / Docker-Compose

Emerald is also available as a docker image, which can be run with dock-compose

```yaml
version: '2.1'
services:
  app:
    image: kahoona/emerald:latest
    ports:
      - "8090:8080"
    environment:
      - BASE_PATH=/emerald
      - DB_FILE=/db/emerald.sqlite
      - TMDB_API_KEY="YOUR_API_KEY"
    volumes:
      - /mnt/media/:/media
      - /opt/emerald/data:/db
``` 
