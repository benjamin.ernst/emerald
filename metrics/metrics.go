package metrics

import "time"

const maxValues = 20

var metrics = make(map[string][]Metric)

type Metric struct {
	Date  time.Time
	Value float32
}

func AddValue(metricName string, val float32) {
	values, ok := metrics[metricName]
	if !ok {
		values = make([]Metric, 0)
	}

	values = append(values, Metric{
		Date:  time.Now(),
		Value: val,
	})

	if len(values) > maxValues {
		//pop first value
		values = append(make([]Metric, 0), values[1:]...)
	}

	metrics[metricName] = values
}
